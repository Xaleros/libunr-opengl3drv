/*===========================================================================*\
|*  libunr-OpenGL3Drv - An OpenGL3 Render Device for libunr                  *|
|*  Copyright (C) 2018-2019  Adam W.E. Smith                                 *|
|*                                                                           *|
|*  This program is free software: you can redistribute it and/or modify     *|
|*  it under the terms of the GNU General Public License as published by     *|
|*  the Free Software Foundation, either version 3 of the License, or        *|
|*  (at your option) any later version.                                      *|
|*                                                                           *|
|*  This program is distributed in the hope that it will be useful,          *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *|
|*  GNU General Public License for more details.                             *|
|*                                                                           *|
|*  You should have received a copy of the GNU General Public License        *|
|*  along with this program. If not, see <https://www.gnu.org/licenses/>.    *|
\*===========================================================================*/

/*========================================================================
 * OpenGL3Shader.h - OpenGL 2.x shader object
 *
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#pragma once

#include <Util/FString.h>
#include <Util/TArray.h>
#include <Util/FFileArchive.h>

// Vertex shaders
#define SHADER_VERTEX_DEFAULT 0

// Fragment shaders
#define SHADER_FRAG_DEFAULT 0
#define SHADER_FRAG_DETAIL  1
#define SHADER_FRAG_TRANSLUCENT 2
#define SHADER_FRAG_MODULATED 3

/*-----------------------------------------------------------------------------
 * FGLShader
 * A single GLSL shader
-----------------------------------------------------------------------------*/
class FGLShader
{
public:
  FGLShader();
  ~FGLShader();

  bool LoadAndCompile( FStringFilePath& Filepath );

  const char* Name;
  u32 Handle;
  bool bCompiled;
};

/*-----------------------------------------------------------------------------
 * FGLShaderProgram
 * A single GLSL program linked with one shader of each type
-----------------------------------------------------------------------------*/
class FGLShaderProgram
{
public:
  FGLShaderProgram();
  ~FGLShaderProgram();

  bool Link();
  void Use();

  u32 Handle;
  bool bLinked;

  FGLShader VertexShader;
  FGLShader FragmentShader;
  FGLShader GeometryShader;

  // Uniforms that every shader should have
  int VUniformMVP;
  int FUniformTex;
  int FUniformDetailTex;
  int FUniformMasked;
};

/*-----------------------------------------------------------------------------
 * FGLShaderManager
 * Manages combinations of vertex and fragment shaders for easy access
-----------------------------------------------------------------------------*/
class FGLShaderManager
{
public:
  FGLShaderManager();
  ~FGLShaderManager();

  bool AddVertexShader( FStringFilePath& FilePath );
  bool AddFragmentShader( FStringFilePath& FilePath );
  void DelProgram( FGLShaderProgram& Program );
  FGLShaderProgram& GetProgram( int VertexShader, int FragmentShader );
  bool CompileAllShaders();

  TArray<FGLShader> VertexShaders;
  TArray<FGLShader> FragmentShaders;
  TArray<FGLShaderProgram> Programs;
};
