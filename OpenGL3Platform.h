/*===========================================================================*\
|*  libunr-OpenGL3Drv - An OpenGL3 Render Device for libunr                  *|
|*  Copyright (C) 2018-2020  Adam W.E. Smith                                 *|
|*                                                                           *|
|*  This program is free software: you can redistribute it and/or modify     *|
|*  it under the terms of the GNU General Public License as published by     *|
|*  the Free Software Foundation, either version 3 of the License, or        *|
|*  (at your option) any later version.                                      *|
|*                                                                           *|
|*  This program is distributed in the hope that it will be useful,          *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *|
|*  GNU General Public License for more details.                             *|
|*                                                                           *|
|*  You should have received a copy of the GNU General Public License        *|
|*  along with this program. If not, see <https://www.gnu.org/licenses/>.    *|
\*===========================================================================*/

/*========================================================================
 * OpenGL3Platform.h - OpenGL Platform support
 *========================================================================
*/

#pragma once

#include <memory>

#include <Util/FMacro.h>
#include <Core/UClass.h>
#include <Engine/UEngine.h>

#if defined LIBUNR_SDL2
#	include <SDL.h>
#	include <SDL_opengl.h>
#	include <Engine/USdlViewport.h>
#endif

#if defined LIBUNR_WIN32
#	include <Windows.h>
#	include <direct.h>
#	include <Engine/UWindowsViewport.h>
#endif

#if defined BUILD_X11
#	include <X11/X.h>
#	include <X11/Xlib.h>
#	include <GL/glx.h>
#	include <Engine/UX11Viewport.h>
#endif

class OpenGL3RenderDevice;

class GlPlatformContext
{
public:
	virtual bool LoadExtensions() = 0;
	virtual void Swap() = 0;
	virtual bool MakeCurrent() = 0;

	virtual ~GlPlatformContext() {}
};

class GlPlatformDriver
{
public:
	virtual bool Match(UViewport * viewport) = 0;

	virtual GlPlatformContext * Create(UOpenGL3RenderDevice * device, UViewport * viewport) = 0;

	const std::string & GetName() const { return name; }

protected:
	GlPlatformDriver(const std::string & name): name(name) {}

	std::string name;
};

template <typename TDriver, typename TViewport, typename TRawContext>
class SharedContextGlPlatformDriver : public GlPlatformDriver
{
public:
	typedef std::shared_ptr<TRawContext> SharedContext;

	SharedContextGlPlatformDriver(const std::string & name):
		GlPlatformDriver(name)
	{}

	bool Match(UViewport * viewport) override;

	GlPlatformContext * Create(UOpenGL3RenderDevice * device, UViewport * genericViewport) override;

protected:
	TDriver & driver() { return static_cast<TDriver &>(*this); }

	std::weak_ptr<TRawContext> mainContext;
};

#if defined LIBUNR_WIN32
class WindowsGlPlatformContext;

class WindowsRawContext
{
public:
	WindowsRawContext(HGLRC rc):
		rc(rc)
	{}

	HGLRC operator *()
	{
		return rc;
	}

	~WindowsRawContext()
	{
		wglMakeCurrent(NULL, NULL);
		wglDeleteContext(rc);
	}
protected:
	HGLRC rc;
};

class WindowsGlPlatformDriver : public SharedContextGlPlatformDriver<WindowsGlPlatformDriver, UWindowsViewport, WindowsRawContext>
{
public:
	WindowsGlPlatformDriver():
		SharedContextGlPlatformDriver("Windows")
	{}

	WindowsGlPlatformContext * CreateMainContext(UOpenGL3RenderDevice * device, UWindowsViewport * viewport, SharedContext & mainContext);

	WindowsGlPlatformContext * CreateSubordinateContext(UOpenGL3RenderDevice * device, UWindowsViewport * viewport, SharedContext & sharedContext);
};

extern WindowsGlPlatformDriver windowsDriver;
#endif /* LIBUNR_WIN32 */

#if defined LIBUNR_SDL2
class Sdl2GlPlatformContext;

class Sdl2RawContext
{
public:
	Sdl2RawContext(SDL_GLContext context):
		context(context)
	{}

	SDL_GLContext operator *() { return context; }

	~Sdl2RawContext()
	{
		SDL_GL_DeleteContext(context);
	}
protected:
	SDL_GLContext context;
};

class Sdl2GlPlatformDriver : public SharedContextGlPlatformDriver<Sdl2GlPlatformDriver, USdlViewport, Sdl2RawContext>
{
public:
	Sdl2GlPlatformDriver():
		SharedContextGlPlatformDriver("SDL2")
	{}

	Sdl2GlPlatformContext * CreateMainContext(UOpenGL3RenderDevice * device, USdlViewport * viewport, SharedContext & mainContext);

	Sdl2GlPlatformContext * CreateSubordinateContext(UOpenGL3RenderDevice * device, USdlViewport * viewport, SharedContext & sharedContext);
};

extern Sdl2GlPlatformDriver sdl2Driver;
#endif /* LIBUNR_SDL2 */

#if defined BUILD_X11
class X11GlPlatformContext;

class X11RawContext
{
public:
	X11RawContext(GLXContext context, Display * display):
		context(context),
		display(display)
	{}

	GLXContext operator *() { return context; }

	~X11RawContext()
	{
		glXDestroyContext(display, context);
	}
protected:
	GLXContext context;
	Display *  display;
};

class X11GlPlatformDriver : public SharedContextGlPlatformDriver<X11GlPlatformDriver, UX11Viewport, X11RawContext>
{
public:
	X11GlPlatformDriver():
		SharedContextGlPlatformDriver("X11")
	{}

	X11GlPlatformContext * CreateMainContext(UOpenGL3RenderDevice * device, UX11Viewport * viewport, SharedContext & mainContext);

	X11GlPlatformContext * CreateSubordinateContext(UOpenGL3RenderDevice * device, UX11Viewport * viewport, SharedContext & sharedContext);
};

extern X11GlPlatformDriver x11Driver;
#endif
