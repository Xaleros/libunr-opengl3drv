/*===========================================================================*\
|*  libunr-OpenGL3Drv - An OpenGL3 Render Device for libunr                  *|
|*  Copyright (C) 2018-2019  Adam W.E. Smith                                 *|
|*                                                                           *|
|*  This program is free software: you can redistribute it and/or modify     *|
|*  it under the terms of the GNU General Public License as published by     *|
|*  the Free Software Foundation, either version 3 of the License, or        *|
|*  (at your option) any later version.                                      *|
|*                                                                           *|
|*  This program is distributed in the hope that it will be useful,          *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *|
|*  GNU General Public License for more details.                             *|
|*                                                                           *|
|*  You should have received a copy of the GNU General Public License        *|
|*  along with this program. If not, see <https://www.gnu.org/licenses/>.    *|
\*===========================================================================*/

/*========================================================================
 * OpenGL3Funcs.cpp - OpenGL extensions used by the renderer
 *
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#include "OpenGL3RenderDevice.h"
#include "OpenGL3Funcs.h"
#include "OpenGL3Platform.h"

#include <Util/FMacro.h>
#include <Util/FLogFile.h>

#if defined LIBUNR_POSIX
#	define LIBUNR_PRIVATE __attribute__((visibility ("hidden")))
#else
#	define LIBUNR_PRIVATE
#endif

// Buffer extensions
LIBUNR_PRIVATE PFNGLGENBUFFERSPROC glGenBuffers = NULL;
LIBUNR_PRIVATE PFNGLBINDBUFFERPROC glBindBuffer = NULL;
LIBUNR_PRIVATE PFNGLBUFFERDATAPROC glBufferData = NULL;
LIBUNR_PRIVATE PFNGLDELETEBUFFERSPROC glDeleteBuffers = NULL;

// Texture extensions
#if defined LIBUNR_WIN32
LIBUNR_PRIVATE PFNGLACTIVETEXTUREPROC glActiveTexture = NULL;
#endif /* defined LIBUNR_WIN32 */

// Blending extensions
LIBUNR_PRIVATE PFNGLBLENDFUNCSEPARATEPROC glBlendFuncSeparate = NULL;
#if defined LIBUNR_WIN32
LIBUNR_PRIVATE PFNGLBLENDEQUATIONPROC     glBlendEquation = NULL;
#endif /* defined LIBUNR_WIN32 */

// Vertex array extensions
LIBUNR_PRIVATE PFNGLGENVERTEXARRAYSPROC glGenVertexArrays = NULL;
LIBUNR_PRIVATE PFNGLBINDVERTEXARRAYPROC glBindVertexArray = NULL;
LIBUNR_PRIVATE PFNGLDELETEVERTEXARRAYSPROC glDeleteVertexArrays = NULL;

// Drawing extensions
#if defined LIBUNR_WIN32
LIBUNR_PRIVATE PFNGLDRAWRANGEELEMENTSPROC glDrawRangeElements = NULL;
#endif /* defined LIBUNR_WIN32 */
LIBUNR_PRIVATE PFNGLPRIMITIVERESTARTINDEXPROC glPrimitiveRestartIndex = NULL;

// Shader extensions
LIBUNR_PRIVATE PFNGLCREATESHADERPROC      glCreateShader = NULL;
LIBUNR_PRIVATE PFNGLSHADERSOURCEPROC      glShaderSource = NULL;
LIBUNR_PRIVATE PFNGLCOMPILESHADERPROC     glCompileShader = NULL;
LIBUNR_PRIVATE PFNGLCREATEPROGRAMPROC     glCreateProgram = NULL;
LIBUNR_PRIVATE PFNGLATTACHSHADERPROC      glAttachShader = NULL;
LIBUNR_PRIVATE PFNGLDELETESHADERPROC      glDeleteShader = NULL;
LIBUNR_PRIVATE PFNGLGETSHADERIVPROC       glGetShaderiv = NULL;
LIBUNR_PRIVATE PFNGLGETSHADERINFOLOGPROC  glGetShaderInfoLog = NULL;
LIBUNR_PRIVATE PFNGLLINKPROGRAMPROC       glLinkProgram = NULL;
LIBUNR_PRIVATE PFNGLUSEPROGRAMPROC        glUseProgram = NULL;
LIBUNR_PRIVATE PFNGLDELETEPROGRAMPROC     glDeleteProgram = NULL;
LIBUNR_PRIVATE PFNGLGETPROGRAMIVPROC      glGetProgramiv = NULL;
LIBUNR_PRIVATE PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog = NULL;

// Vertex attribute extensions
LIBUNR_PRIVATE PFNGLVERTEXATTRIBPOINTERPROC     glVertexAttribPointer = NULL;
LIBUNR_PRIVATE PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray = NULL;
LIBUNR_PRIVATE PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray = NULL;

// Uniform shader extensions
LIBUNR_PRIVATE PFNGLUNIFORM1IPROC glUniform1i = NULL;
LIBUNR_PRIVATE PFNGLUNIFORM3FVPROC glUniform3fv = NULL;
LIBUNR_PRIVATE PFNGLUNIFORM4FPROC glUniform4f = NULL;
LIBUNR_PRIVATE PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv = NULL;
LIBUNR_PRIVATE PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation = NULL;

#define LOAD_EXT_PROC( proc, type ) \
	proc = reinterpret_cast<type>(GetProcAddress(TXT(proc))); \
	if ( proc == NULL ) { \
		GLogf( LOG_ERR, "Failed to get OpenGL extension '%s'", TXT(proc) ); \
		return false; \
	}

bool LoadExtensionsImpl(void* (*GetProcAddress)(const char *))
{
	GLogf( LOG_INFO, "LoadExtensionsImpl");

	LOAD_EXT_PROC( glGenBuffers, PFNGLGENBUFFERSPROC );
	LOAD_EXT_PROC( glBindBuffer, PFNGLBINDBUFFERPROC );
	LOAD_EXT_PROC( glBufferData, PFNGLBUFFERDATAPROC );
	LOAD_EXT_PROC( glDeleteBuffers, PFNGLDELETEBUFFERSPROC );

#if defined LIBUNR_WIN32
	LOAD_EXT_PROC( glActiveTexture, PFNGLACTIVETEXTUREPROC );
#endif /* defined LIBUNR_WIN32 */

	LOAD_EXT_PROC( glBlendFuncSeparate, PFNGLBLENDFUNCSEPARATEPROC );
#if defined LIBUNR_WIN32
	LOAD_EXT_PROC( glBlendEquation, PFNGLBLENDEQUATIONPROC );
#endif /* defined LIBUNR_WIN32 */

	LOAD_EXT_PROC( glGenVertexArrays, PFNGLGENVERTEXARRAYSPROC );
	LOAD_EXT_PROC( glBindVertexArray, PFNGLBINDVERTEXARRAYPROC );
	LOAD_EXT_PROC( glDeleteVertexArrays, PFNGLDELETEVERTEXARRAYSPROC );

#if defined LIBUNR_WIN32
	LOAD_EXT_PROC( glDrawRangeElements, PFNGLDRAWRANGEELEMENTSPROC );
#endif /* defined LIBUNR_WIN32 */
	LOAD_EXT_PROC( glPrimitiveRestartIndex, PFNGLPRIMITIVERESTARTINDEXPROC );

	LOAD_EXT_PROC( glCreateShader, PFNGLCREATESHADERPROC );
	LOAD_EXT_PROC( glShaderSource, PFNGLSHADERSOURCEPROC );
	LOAD_EXT_PROC( glCompileShader, PFNGLCOMPILESHADERPROC );
	LOAD_EXT_PROC( glCreateProgram, PFNGLCREATEPROGRAMPROC );
	LOAD_EXT_PROC( glAttachShader, PFNGLATTACHSHADERPROC );
	LOAD_EXT_PROC( glDeleteShader, PFNGLDELETESHADERPROC );
	LOAD_EXT_PROC( glGetShaderiv, PFNGLGETSHADERIVPROC );
	LOAD_EXT_PROC( glGetShaderInfoLog, PFNGLGETSHADERINFOLOGPROC );
	LOAD_EXT_PROC( glLinkProgram, PFNGLLINKPROGRAMPROC );
	LOAD_EXT_PROC( glUseProgram, PFNGLUSEPROGRAMPROC );
	LOAD_EXT_PROC( glDeleteProgram, PFNGLDELETEPROGRAMPROC );
	LOAD_EXT_PROC( glGetProgramiv, PFNGLGETPROGRAMIVPROC );
	LOAD_EXT_PROC( glGetProgramInfoLog, PFNGLGETPROGRAMINFOLOGPROC );

	LOAD_EXT_PROC( glVertexAttribPointer, PFNGLVERTEXATTRIBPOINTERPROC );
	LOAD_EXT_PROC( glEnableVertexAttribArray, PFNGLENABLEVERTEXATTRIBARRAYPROC );
	LOAD_EXT_PROC( glDisableVertexAttribArray, PFNGLDISABLEVERTEXATTRIBARRAYPROC );

	LOAD_EXT_PROC( glUniform1i, PFNGLUNIFORM1IPROC );
	LOAD_EXT_PROC( glUniform3fv, PFNGLUNIFORM3FVPROC );
	LOAD_EXT_PROC( glUniform4f, PFNGLUNIFORM4FPROC );
	LOAD_EXT_PROC( glUniformMatrix4fv, PFNGLUNIFORMMATRIX4FVPROC );
	LOAD_EXT_PROC( glGetUniformLocation, PFNGLGETUNIFORMLOCATIONPROC );

	return true;
}
