/*===========================================================================*\
|*  libunr-OpenGL3Drv - An OpenGL3 Render Device for libunr                  *|
|*  Copyright (C) 2018-2019  Adam W.E. Smith                                 *|
|*                                                                           *|
|*  This program is free software: you can redistribute it and/or modify     *|
|*  it under the terms of the GNU General Public License as published by     *|
|*  the Free Software Foundation, either version 3 of the License, or        *|
|*  (at your option) any later version.                                      *|
|*                                                                           *|
|*  This program is distributed in the hope that it will be useful,          *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *|
|*  GNU General Public License for more details.                             *|
|*                                                                           *|
|*  You should have received a copy of the GNU General Public License        *|
|*  along with this program. If not, see <https://www.gnu.org/licenses/>.    *|
\*===========================================================================*/

/*========================================================================
 * OpenGL3Shader.h - OpenGL 2.x shader object
 *
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#include "OpenGL3Shader.h"
#include "OpenGL3Funcs.h"

#include <Util/FLogFile.h>

/*-----------------------------------------------------------------------------
 * FGLShader
-----------------------------------------------------------------------------*/
FGLShader::FGLShader()
{
  Name = NULL;
  Handle = 0;
  bCompiled = false;
}

FGLShader::~FGLShader()
{
}

bool FGLShader::LoadAndCompile( FStringFilePath& Filepath )
{
  FFileArchiveIn In;
  GLenum ShaderType;

  // Figure out the shader type
  Name = Filepath.GetName();
  const char* Ext = Filepath.GetExt();
  if ( Ext == NULL )
  {
  UnknownShaderType:
    GLogf( LOG_ERR, "Unknown shader type for file '%s", Name );
    return false;
  }

  if ( strcmp( Ext, "vert" ) == 0 )
    ShaderType = GL_VERTEX_SHADER;
  else if ( strcmp( Ext, "frag" ) == 0 )
    ShaderType = GL_FRAGMENT_SHADER;
  else if ( strcmp( Ext, "geom" ) == 0 )
    ShaderType = GL_GEOMETRY_SHADER;
  else
    goto UnknownShaderType;

  Handle = glCreateShader( ShaderType );
  Name = strdup( Filepath.GetName() );

  // Open shader code
  if ( In.Open( Filepath.Data() ) < 0 )
  {
    GLogf( LOG_ERR, "Can't open shader file '%s'", Filepath.Data() );
    glDeleteShader( Handle );
    Handle = 0;
    return false;
  }

  // Read in the whole file
  In.Seek( 0, End );
  int Len = In.Tell();
  In.Seek( 0, Begin );

  char* Code = new char[Len];
  In.Read( Code, Len );

  // Attempt to compile shader
  glShaderSource( Handle, 1, &Code, &Len );
  glCompileShader( Handle );

  // Check for failure
  int CompilationSuccess;
  char Log[512];
  glGetShaderiv( Handle, GL_COMPILE_STATUS, &CompilationSuccess );
  if ( !CompilationSuccess )
  {
    glGetShaderInfoLog( Handle, 512, NULL, Log );
    GLogf( LOG_ERR, "Failed to compile shader '%s'", Name );
    GLogf( LOG_ERR, Log );
    delete[] Code;
    glDeleteShader( Handle );
    Handle = 0;
    return false;
  }

  bCompiled = true;
  return true;
}

/*-----------------------------------------------------------------------------
 * FGLShaderProgram
-----------------------------------------------------------------------------*/
FGLShaderProgram::FGLShaderProgram()
{
  Handle = 0;
  bLinked = false;
}

FGLShaderProgram::~FGLShaderProgram()
{
}

bool FGLShaderProgram::Link()
{
  int LinkSuccess = 0;
  char Log[512];
  
  Handle = glCreateProgram();

  // Attach vertex shader
  if ( VertexShader.bCompiled )
  {
    glAttachShader( Handle, VertexShader.Handle );
  }
  else
  {
    GLogf( LOG_ERR, "No vertex shader specified in program" );
    return false;
  }

  // Attach fragment shader
  if ( FragmentShader.bCompiled )
  {
    glAttachShader( Handle, FragmentShader.Handle );
  }
  else
  {
    GLogf( LOG_ERR, "No fragment shader specified in program" );
    return false;
  }

  // Attach optional geometry shader
  if ( GeometryShader.bCompiled )
    glAttachShader( Handle, GeometryShader.Handle );

  glLinkProgram( Handle );
  glGetProgramiv( Handle, GL_LINK_STATUS, &LinkSuccess );
  if ( !LinkSuccess )
  {
    glGetProgramInfoLog( Handle, 512, NULL, Log );
    GLogf( LOG_ERR, "Failed to link shader program '%s'", VertexShader.Name );
    GLogf( LOG_ERR, Log );
    glDeleteProgram( Handle );
    Handle = 0;
    return false;
  }

  bLinked = true;

  // Get uniform locations
  VUniformMVP = glGetUniformLocation( Handle, "MVP" );
  if ( glGetError() == GL_INVALID_VALUE )
    GLogf( LOG_WARN, "Shader program '%s' does not have uniform mat4 MVP", VertexShader.Name );
  
  FUniformTex = glGetUniformLocation( Handle, "Tex" );
  if ( glGetError() == GL_INVALID_VALUE )
    GLogf( LOG_WARN, "Shader program '%s' does not have uniform sampler2D Tex", FragmentShader.Name );

  FUniformDetailTex = glGetUniformLocation( Handle, "DetailTex" );
  if ( glGetError() == GL_INVALID_VALUE )
    GLogf( LOG_WARN, "Shader program '%s' does not have uniform sampler2D DetailTex", FragmentShader.Name );

  FUniformMasked = glGetUniformLocation( Handle, "bMasked" );
  if ( glGetError() == GL_INVALID_VALUE )
    GLogf( LOG_WARN, "Shader program '%s' does not have uniform bool bMasked", FragmentShader.Name );

  return true;
}

void FGLShaderProgram::Use()
{
  glUseProgram( Handle );
}

/*-----------------------------------------------------------------------------
 * FGLShaderManager
-----------------------------------------------------------------------------*/
FGLShaderManager::FGLShaderManager()
{
  VertexShaders.Reserve( 4 );
  FragmentShaders.Reserve( 4 );
  Programs.Reserve( 16 );
}

FGLShaderManager::~FGLShaderManager()
{
  int i;
  for ( i = 0; i < Programs.Size(); i++ )
    glDeleteProgram( Programs[i].Handle );
  
  for ( i = 0; i < FragmentShaders.Size(); i++ )
    glDeleteShader( FragmentShaders[i].Handle );

  for ( i = 0; i < VertexShaders.Size(); i++ )
    glDeleteShader( VertexShaders[i].Handle );
}

bool FGLShaderManager::AddVertexShader( FStringFilePath& FilePath )
{
  FGLShader InShader;
  
  // Try to build
  if ( !InShader.LoadAndCompile( FilePath ) )
    return false;

  // Add to the array
  VertexShaders.PushBack( InShader );

  return true;
}

bool FGLShaderManager::AddFragmentShader( FStringFilePath& FilePath )
{
  FGLShader InShader;

  // Try to build
  if ( !InShader.LoadAndCompile( FilePath ) )
    return false;

  // Add to the array
  FragmentShaders.PushBack( InShader );

  return true;
}

void FGLShaderManager::DelProgram( FGLShaderProgram& Program )
{
  if ( !Program.bLinked )
    return;

  for ( int i = 0; i < Programs.Size(); i++ )
  {
    if ( Programs[i].Handle == Program.Handle )
      Programs.Erase( i );
  }
}

FGLShaderProgram& FGLShaderManager::GetProgram( int VertexShader, int FragmentShader )
{
  if ( VertexShader >= VertexShaders.Size() || FragmentShader >= FragmentShaders.Size() )
    return Programs[0]; // Return the default shader for out of bounds accesses

  // Calculate where the shader programs for the given vertex shader begin
  int StartIndex = VertexShader * VertexShaders.Size();

  // And factor in the fragment shader to get the corresponding program for this shader combo
  return Programs[StartIndex + FragmentShader];
}

bool FGLShaderManager::CompileAllShaders()
{
  // Compile a shader program for each vertex and fragment shader combination
  for ( int i = 0; i < VertexShaders.Size(); i++ )
  {
    for ( int j = 0; j < FragmentShaders.Size(); j++ )
    {
      FGLShaderProgram Program;
      Program.VertexShader = VertexShaders[i];
      Program.FragmentShader = FragmentShaders[j];

      if ( !Program.Link() )
        return false;

      Programs.PushBack( Program );
    }
  }
  return true;
}
