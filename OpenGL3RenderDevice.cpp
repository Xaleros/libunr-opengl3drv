/*===========================================================================*\
|*  libunr-OpenGL3Drv - An OpenGL3 Render Device for libunr                  *|
|*  Copyright (C) 2018-2019  Adam W.E. Smith                                 *|
|*                                                                           *|
|*  This program is free software: you can redistribute it and/or modify     *|
|*  it under the terms of the GNU General Public License as published by     *|
|*  the Free Software Foundation, either version 3 of the License, or        *|
|*  (at your option) any later version.                                      *|
|*                                                                           *|
|*  This program is distributed in the hope that it will be useful,          *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *|
|*  GNU General Public License for more details.                             *|
|*                                                                           *|
|*  You should have received a copy of the GNU General Public License        *|
|*  along with this program. If not, see <https://www.gnu.org/licenses/>.    *|
\*===========================================================================*/

/*========================================================================
 * OpenGL3RenderDevice.cpp - OpenGL 3.x 3D rendering device
 *
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#include <Core/UClass.h>
#include <Core/UMath.h>
#include <Engine/UEngine.h>
#include <Engine/ULodMesh.h>
#include <Engine/UModel.h>
#include <Engine/ULodMesh.h>
#include <Actors/APlayerPawn.h>

#include "OpenGL3Funcs.h"

#if defined LIBUNR_WIN32
#	include <Engine/UWindowsViewport.h>
#elif defined LIBUNR_POSIX
#	include <Engine/UX11Viewport.h>
#endif /* defined LIBUNR_WIN32 */

#include "OpenGL3RenderDevice.h"
#include "OpenGL3Platform.h"

#if defined LIBUNR_WIN32
#	define VOIDP_TO_GLUINT(x) ((GLuint)x)
#	define GLUINT_TO_VOIDP(x) ((void *)x)
#else
#	define VOIDP_TO_GLUINT(x) (static_cast<GLuint>(reinterpret_cast<intptr_t>(x)))
#	define GLUINT_TO_VOIDP(x) (reinterpret_cast<void *>(static_cast<intptr_t>(x)))
#endif

/*-----------------------------------------------------------------------------
 * Render Device Constructor
-----------------------------------------------------------------------------*/
UOpenGL3RenderDevice::UOpenGL3RenderDevice()
  : URenderDevice()
{
}

/*-----------------------------------------------------------------------------
 * Render Device Destructor
-----------------------------------------------------------------------------*/
UOpenGL3RenderDevice::~UOpenGL3RenderDevice()
{
}

/*-----------------------------------------------------------------------------
 * Render Device Initialization
 * 
 * We do different things here based on what operating systems we can run on
 *
 * For Windows, we do the following
 * - Create a "fake" window for our "fake" OpenGL context
 * - Set up a "fake" OpenGL context so that we can get extension functions
 *
 * After platform specific init, we do the following
 * - Get any extension functions that we require
 * - Set global GL state that won't change after context creation
-----------------------------------------------------------------------------*/
bool UOpenGL3RenderDevice::Init()
{
  // Load config options
  bAccelerateFractalTextures = GLibunrConfig->ReadBool( "OpenGL3Drv.OpenGL3RenderDevice", "bAccelerateFractalTextures", 0, false );

  return true;
}


/*-----------------------------------------------------------------------------
 * Render Device Viewport Initialization
 *
 * This is largely platform dependent, but has some platform independent code
 *
 * For Windows, we do the following
 * - Make sure the viewport instance type matches our target platform
 * - Get a pixel format that matches what we want for this viewport
 * - Set the pixel format if we have one available
 * - Set our context attributes to target OpenGL 2.1
 * - If we don't have a main rendering context yet, assign it
 * - If we do, make sure all of our resources are shared across contexts
 * - Make our new context the current one
 * - If we're setting up the main context, compile our default shaders
 *
 * After platform specific init, do the following
 * - Set our new GL viewport to match our viewport size
 * - Enable color and depth buffers
 * - Calculate our perspective and orthographic matrices once for later reuse
 * - Push our new viewport into an array
-----------------------------------------------------------------------------*/
bool UOpenGL3RenderDevice::InitViewport( UViewport* Viewport )
{
  FGLViewport GLViewport;
  GlPlatformContext * context = nullptr;

#if defined LIBUNR_SDL2
  if (context == nullptr && sdl2Driver.Match(Viewport))
  {
    context = sdl2Driver.Create(this, Viewport);
  }
#endif
#if defined LIBUNR_WIN32
  if (context == nullptr && windowsDriver.Match(Viewport))
  {
	context = windowsDriver.Create(this, Viewport);
  }
#endif
#if defined BUILD_X11
  if (context == nullptr && x11Driver.Match(Viewport))
  {
	context = x11Driver.Create(this, Viewport);
  }
#endif

  if (context == nullptr)
  {
    GLogf( LOG_CRIT, "Unsupported viewport type '%s", Viewport->Class->Name.Data() );
    return false;
  }

  GLViewport.Viewport = Viewport;
  GLViewport.context = context;

  // Init viewport
  glViewport( 0, 0, Viewport->Width, Viewport->Height );

  // Set global state properties
  glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );

  glEnable( GL_BLEND );
  glBlendFunc( GL_ONE, GL_ZERO );

  glEnable( GL_DEPTH_TEST );
  glDepthFunc( GL_LESS );

  glEnable( GL_CULL_FACE );

  // Setup viewport projection matrices
  GetPerspectiveMatrix( GLViewport.Perspective, 90.f, Viewport->Width, Viewport->Height, ZNEAR, ZFAR );
  GetOrthoMatrix( GLViewport.Orthographic, 0.0f, Viewport->Width, 0.0f, Viewport->Height, -1.0f, 1.0f );

  // Keep track of our new viewport
  Viewports.PushBack( GLViewport );
  CurrentViewport = &Viewports[Viewports.Size() - 1];

  return true;
}

bool UOpenGL3RenderDevice::SetActiveViewport( UViewport* Viewport )
{
  return false;
}

/*-----------------------------------------------------------------------------
 * Render Device Exit
 *
 * Here, we free all GL contexts that have been created
 * This is entirely platform dependent
-----------------------------------------------------------------------------*/

bool UOpenGL3RenderDevice::Exit()
{
	while (!Viewports.IsEmpty())
	{
		auto& viewport = Viewports.Back();

		delete viewport.context;

		Viewports.PopBack();
	}

	return true;
}

/*-----------------------------------------------------------------------------
 * Render Device Tick
 *
 * The general order of operations is as follows.
 * - Clear out the currently active framebuffer
 * - Draw the world from the perspective of the camera attached to the viewport
 * - Generate vertex buffers based on the number of elements we have
 * - Draw all BSP surfaces
 * - Draw all actors and meshes
 * - Draw each canvas element
 * - Draw transparent surfaces in order
 * - Swap framebuffers so that we can see the results of our frame
-----------------------------------------------------------------------------*/
void UOpenGL3RenderDevice::Tick( float DeltaTime )
{
  Super::Tick( DeltaTime );

  // Clear out current framebuffer
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

  // Create vertex array
  u32 VAO;
  glGenVertexArrays( 1, &VAO );
  glBindVertexArray( VAO );

  // Set up vertex pointer structure

  // Draw world
  DrawWorld( GEngine->Level, CurrentViewport->Viewport );

  // Reuse old buffers so we aren't generating/freeing buffers constantly
  NumBuffers++; // For clipping plane debug
  if ( VertexBuffers.Size() < NumBuffers )
  {
    int OldSize = VertexBuffers.Size();
    VertexBuffers.Resize( NumBuffers, 0 );
    IndexBuffers.Resize( NumBuffers, 0 );

    glGenBuffers( NumBuffers - OldSize, &VertexBuffers[OldSize] );
    glGenBuffers( NumBuffers - OldSize, &IndexBuffers[OldSize] );
  }
  else if ( NumBuffers < (VertexBuffers.Size() / 2) )
  {
    // Free up some buffers if we have way more than needed
    int NumDeleted = VertexBuffers.Size() / 2;
    glDeleteBuffers( NumDeleted, &VertexBuffers[NumDeleted] );
    glDeleteBuffers( NumDeleted, &IndexBuffers[NumDeleted] );

    VertexBuffers.Resize( NumDeleted );
    IndexBuffers.Resize( NumDeleted );
  }

  CurrentBuffer = 0;

  // Draw BSP elements
  RenderBspElements();

  // Draw arbitrary mesh elements
  RenderMeshElements();

  // Draw canvas elements
  RenderCanvasElements();

  // Draw remaining transparent elements
  RenderTransparentElements();

  // Clean up vertex array
  glDeleteVertexArrays( 1, &VAO );
  NumBuffers = 0;

  // Swap buffers to show results of this frame
  CurrentViewport->context->Swap();
}

/*-----------------------------------------------------------------------------
 * Render Device Draw Canvas Elements
 * Simply loops through each canvas element and draws it on the screen
-----------------------------------------------------------------------------*/
void UOpenGL3RenderDevice::RenderCanvasElements()
{
  // Draw any queued canvas element
  for ( int i = CurrentBuffer; i < NumBuffers; i++ )
  {
    if (CanvasDrawList.Size() < 1)
    {
      GLogf( LOG_CRIT, "No canvases for buffer %d", i);
      break;
    }

    FCanvasElement& Element = CanvasDrawList.Front();

    switch ( Element.Type )
    {
      case ELEM_Tile:
        RenderTileElement( Element, i );
        break;
      case ELEM_VertexMesh:
        RenderVertexMeshElement( Element, i );
        break;
      default:
        break;
    }

    // Remove element from queue
    CanvasDrawList.Pop();
    if ( CanvasDrawList.IsEmpty() )
      break; 

    CurrentBuffer++;
  }
}

/*-----------------------------------------------------------------------------
 * Render Device Draw Canvas Elements
 * Renders a single canvas tile on the screen
-----------------------------------------------------------------------------*/
void UOpenGL3RenderDevice::RenderTileElement( FCanvasElement& Element, int BufIdx )
{
  if ( !Element.Texture )
  {
    GLogf(LOG_ERR, "Null texture");
    return;
  }

  FGLShaderProgram& Program = SetPolygonEffects( Element.Texture, Element.VertexShader, Element.FragmentShader, Element.PolyFlags );

  // Set correct projection and view matrix
  FMatrix4x4& CurrentMat = CurrentViewport->Orthographic;
  glUniformMatrix4fv( Program.VUniformMVP, 1, false, (GLfloat*)CurrentMat.Data );

  // Push vertices into a buffer
  glBindBuffer( GL_ARRAY_BUFFER, VertexBuffers[BufIdx] );
  glBufferData( GL_ARRAY_BUFFER, Element.Vertices.Size() * sizeof( FVertexUV ), Element.Vertices.Data(), GL_STATIC_DRAW );

  // TODO: Move this out after sorting by shader program has been implemented
  glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof( GLfloat ), 0 );
  glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof( GLfloat ), (GLvoid*)(3 * sizeof( GLfloat )) );
  glEnableVertexAttribArray( 0 );
  glEnableVertexAttribArray( 1 );

  // Push indices into a buffer
  glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, IndexBuffers[BufIdx] );
  glBufferData( GL_ELEMENT_ARRAY_BUFFER, Element.Indices.Size() * sizeof( u32 ), Element.Indices.Data(), GL_STATIC_DRAW );

  // Draw element
  glDrawElements( GL_TRIANGLES, Element.Indices.Size(), GL_UNSIGNED_INT, 0 );

  // TODO: Move this out after sorting by shader program has been implemented
  glDisableVertexAttribArray( 0 );
  glDisableVertexAttribArray( 1 );
}

/*-----------------------------------------------------------------------------
 * Render Device Draw Canvas Elements
 * Renders a single canvas vertex mesh on the screen
-----------------------------------------------------------------------------*/
void UOpenGL3RenderDevice::RenderVertexMeshElement( FCanvasElement& Element, int BufIdx )
{
}

/*-----------------------------------------------------------------------------
 * Render Device Bsp Rendering
 * Adds a single BSP node to the draw list after being processed in the tree
-----------------------------------------------------------------------------*/
void UOpenGL3RenderDevice::DrawBspSurface( UModel* Model, FBspNode& Node, UViewport* Viewport )
{
  if ( Node.NumVertices == 0 )
    return;

  if ( Node.iSurf < 0 || Model->Surfs.Size() <= Node.iSurf )
  {
    GLogf(LOG_WARN, "Node.iSurf %d out of bounds", Node.iSurf);
    return;
  }

  if ( !Model )
  {
    GLogf(LOG_WARN, "Null Model");
    return;
  }

  FBspSurf& Surf = Model->Surfs[Node.iSurf];

  if ( Surf.PolyFlags & PF_Invisible )
    return; // Invisible, don't render

  if ( !Viewport )
  {
    GLogf(LOG_WARN, "Null Viewport");
    return;
  }

  if ( !Viewport->Actor )
  {
    GLogf(LOG_WARN, "Null Viewport Actor");
    return;
  }

  if ( !(Surf.PolyFlags & PF_TwoSided) && Dot( Viewport->Actor->Location, Node.Plane ) < 0.0f )
    return; // We're not on the visible side of the sheet, don't render
  if ( Surf.PolyFlags & (PF_Translucent | PF_Modulated) )
  {
    DrawTransparentBspSurface( Model, Node, Viewport );
    return;
  }

  FVert* BspVert = &Model->Verts[Node.iVertPool];
  FVector& U = Model->Vectors[Surf.vTextureU];
  FVector& V = Model->Vectors[Surf.vTextureV];
  FVector& Base = Model->Points[Surf.pBase];
  int USize = 0;
  int VSize = 0;

  if ( Surf.Texture )
  {
    USize = Surf.Texture->USize;
    VSize = Surf.Texture->VSize;
  }

  if ( USize == 0 || VSize == 0 )
  {
    // Avoid division by zero later
    USize = 64;
    VSize = 64;
  }

  // Find bsp element
  FBspPolySet* Element = NULL;
  for ( int i = 0; i < BspPolyData.Size(); i++ )
  {
    FBspPolySet& E = BspPolyData[i];
    if ( E.Texture == Surf.Texture && E.PolyFlags == Surf.PolyFlags )
    {
      Element = &E;
      break;
    }
  }

  if ( Element == NULL )
  {
    BspPolyData.Resize( BspPolyData.Size() + 1 );
    Element = &BspPolyData.At(BspPolyData.Size() - 1);
    Element->Texture = Surf.Texture;
    Element->FragmentShader = SHADER_FRAG_DEFAULT;
    Element->PolyFlags = Surf.PolyFlags;
    NumBuffers++;
  }

  // TODO: Actors
  // TODO: Decals
  // TODO: Projectors

  // Set up vertices
  FVertexUV Vert;
  u32 InitFan = Element->Vertices.Size(); // Used later for indices
  Element->Vertices.Resize( Element->Vertices.Size() + Node.NumVertices );
  for ( int i = 0; i < Node.NumVertices; i++ )
  {
    FVertexUV& Vert = Element->Vertices[InitFan + i];
    FVector& Vertex = Model->Points[BspVert[i].pVertex];
    FVector Dist;
    Dist.X = Vertex.X - Base.X;
    Dist.Y = Vertex.Y - Base.Y;
    Dist.Z = Vertex.Z - Base.Z;

    float TexU = (Dot( Dist, U ) + (float)Surf.PanU) / (float)USize;
    float TexV = (Dot( Dist, V ) + (float)Surf.PanV) / (float)VSize;

    Vert.X = -Vertex.Y;
    Vert.Y = Vertex.Z;
    Vert.Z = Vertex.X;
    Vert.U = TexU;
    Vert.V = TexV;
  }

  // Set up triangles
  GLuint TempI[3];
  for ( int i = 0; i < Node.NumVertices - 2; i++ )
  {
    TempI[0] = InitFan + i + 2;
    TempI[1] = InitFan + i + 1;
    TempI[2] = InitFan;
    Element->Indices.Append( TempI, 3 );
  }
}


/*-----------------------------------------------------------------------------
 * Render Device Draw Transparent BSP
 * Adds a single transparent BSP surface to the transparent poly list
-----------------------------------------------------------------------------*/
void UOpenGL3RenderDevice::RenderTransparentElements()
{
  // Disable depth writes
  glDepthMask( GL_FALSE );

  // Go through all bsp polygon sets
  for ( int i = 0; i < TransparentPolyData.Size(); i++ )
  {
    FTransparentElement& Element = TransparentPolyData[i];

    if ( !Element.Texture )
    {
      GLogf(LOG_ERR, "Null texture");
      continue;
    }

    FGLShaderProgram& Program = SetPolygonEffects( Element.Texture, Element.VertexShader, Element.FragmentShader, Element.PolyFlags );

    // Construct MVP matrix
    FMatrix4x4 MVP;
    MVP = ViewMatrix;
    MVP *= CurrentViewport->Perspective;

    glUniformMatrix4fv( Program.VUniformMVP, 1, false, (GLfloat*)MVP.Data );

    // Push vertices into a buffer
    glBindBuffer( GL_ARRAY_BUFFER, VertexBuffers[i] );
    glBufferData( GL_ARRAY_BUFFER, Element.Vertices.Size() * sizeof( FVertexUV ), Element.Vertices.Data(), GL_STATIC_DRAW );
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof( GLfloat ), 0 );
    glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof( GLfloat ), (GLvoid*)(3 * sizeof( GLfloat )) );
    glEnableVertexAttribArray( 0 );
    glEnableVertexAttribArray( 1 );

    // Push indices into a buffer
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, IndexBuffers[i] );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, Element.Indices.Size() * sizeof( u32 ), Element.Indices.Data(), GL_STATIC_DRAW );

    // Draw all elements
    glDrawElements( GL_TRIANGLES, Element.Indices.Size(), GL_UNSIGNED_INT, 0 );

    // Remove element from queue
    CurrentBuffer++;
  }

  // Re-enable depth writes
  glDepthMask( GL_TRUE );
  TransparentPolyData.Clear();
}

void UOpenGL3RenderDevice::DrawTransparentBspSurface( UModel* Model, FBspNode& Node, UViewport* Viewport )
{
  FBspSurf& Surf = Model->Surfs[Node.iSurf];
  FVert* BspVert = &Model->Verts[Node.iVertPool];
  FVector& U = Model->Vectors[Surf.vTextureU];
  FVector& V = Model->Vectors[Surf.vTextureV];
  FVector& Base = Model->Points[Surf.pBase];
  int USize = Surf.Texture->USize;
  int VSize = Surf.Texture->VSize;

  if ( USize == 0 || VSize == 0 )
  {
    // Avoid division by zero later
    USize = 64;
    VSize = 64;
  }

  FTransparentElement Element;
  Element.Texture = Surf.Texture;
  Element.ElementType = ELEM_Bsp;
  Element.VertexShader = SHADER_VERTEX_DEFAULT;
  Element.FragmentShader = SHADER_FRAG_TRANSLUCENT;
  Element.PolyFlags = Surf.PolyFlags;

  FVertexUV Vert;
  u32 InitFan = 0;
  FVector Distance;
  Element.Vertices.Resize( Node.NumVertices );
  for ( int i = 0; i < Node.NumVertices; i++ )
  {
    FVertexUV& Vert = Element.Vertices[InitFan + i];
    FVector& Vertex = Model->Points[BspVert[i].pVertex];
    FVector Dist;
    Dist.X = Vertex.X - Base.X;
    Dist.Y = Vertex.Y - Base.Y;
    Dist.Z = Vertex.Z - Base.Z;

    float TexU = (Dot( Dist, U ) + (float)Surf.PanU) / (float)USize;
    float TexV = (Dot( Dist, V ) + (float)Surf.PanV) / (float)VSize;

    Vert.X = -Vertex.Y;
    Vert.Y = Vertex.Z;
    Vert.Z = Vertex.X;
    Vert.U = TexU;
    Vert.V = TexV;

    Distance.X += Vert.X;
    Distance.Y += Vert.Y;
    Distance.Z += Vert.Z;
  }

  Distance *= (1.0 / Node.NumVertices);
  Element.Distance = Distance.VSize() - Viewport->Actor->Location.VSize();

  // Set up triangles
  GLuint TempI[3];
  for ( int i = 0; i < Node.NumVertices - 2; i++ )
  {
    TempI[0] = InitFan + i + 2;
    TempI[1] = InitFan + i + 1;
    TempI[2] = InitFan;
    Element.Indices.Append( TempI, 3 );
  }

  //if ( TransparentPolyData.Size() == 0 )
  //{
    TransparentPolyData.PushBack( Element );
  //  return;
  //}

  // Insert based on distance
  //for ( int i = 0; i < TransparentPolyData.Size(); i++ )
  //{
  //  FTransparentElement It = TransparentPolyData[i];
  //  if ( It.Distance <= Element.Distance )
  //  {
  //    TransparentPolyData.Insert( Element, i );
  //    break;
  //  }
  //}

  NumBuffers++;
}

/*-----------------------------------------------------------------------------
 * Render Device Draw BSP elements
 * Renders all groups of BSP elements at once
-----------------------------------------------------------------------------*/
void UOpenGL3RenderDevice::RenderBspElements()
{
  // Go through all bsp polygon sets
  for ( int i = 0; i < BspPolyData.Size(); i++ )
  {
    FBspPolySet& Element = BspPolyData[i];

    if ( !Element.Texture )
    {
      GLogf(LOG_ERR, "Null texture");
      continue;
    }

    FGLShaderProgram& Program = SetPolygonEffects( Element.Texture, SHADER_VERTEX_DEFAULT, Element.FragmentShader, Element.PolyFlags );

    // Construct MVP matrix
    FMatrix4x4 MVP;
    MVP = ViewMatrix;
    MVP *= CurrentViewport->Perspective;

    glUniformMatrix4fv( Program.VUniformMVP, 1, false, (GLfloat*)MVP.Data );

    // Push vertices into a buffer
    glBindBuffer( GL_ARRAY_BUFFER, VertexBuffers[i] );
    glBufferData( GL_ARRAY_BUFFER, Element.Vertices.Size() * sizeof( FVertexUV ), Element.Vertices.Data(), GL_STATIC_DRAW );
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof( GLfloat ), 0 );
    glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof( GLfloat ), (GLvoid*)(3 * sizeof( GLfloat )) );
    glEnableVertexAttribArray( 0 );
    glEnableVertexAttribArray( 1 );

    // Push indices into a buffer
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, IndexBuffers[i] );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, Element.Indices.Size() * sizeof( u32 ), Element.Indices.Data(), GL_STATIC_DRAW );

    // Draw all elements
    glDrawElements( GL_TRIANGLES, Element.Indices.Size(), GL_UNSIGNED_INT, 0 );

    // Remove element from queue
    CurrentBuffer++;
  }

  BspPolyData.Clear();
}

// TODO:
void UOpenGL3RenderDevice::DrawActor( AActor* Actor )
{
}

/*-----------------------------------------------------------------------------
 * Render Device Arbitrary Mesh Rendering
 *
 * This just draws a mesh straight into the world without any interactivity
 * with the world around it. Usually only used for testing mesh drawing
 * or for the mesh viewer as seen in the editor
 -----------------------------------------------------------------------------*/
void UOpenGL3RenderDevice::RenderMeshElements()
{
  if ( MeshDrawList.IsEmpty() )
    return;

  // Draw any queued canvas element
  for ( int i = CurrentBuffer; i < NumBuffers; i++ )
  {
    FMeshElement& Element = MeshDrawList.Front();

    switch ( Element.Type )
    {
      case MESH_Primitive:
        RenderPrimitiveMesh( Element, i );
        break;
      case MESH_Vertex:
        RenderLodMesh( Element, i );
        break;
    }

    // Remove element from queue
    CurrentBuffer++;
    MeshDrawList.Pop();
    if ( MeshDrawList.IsEmpty() )
      break;
  }
}

/*-----------------------------------------------------------------------------
 * Render Device Primitive Mesh Rendering
 * Renders a simple mesh with no extra animation 
-----------------------------------------------------------------------------*/
void UOpenGL3RenderDevice::RenderPrimitiveMesh( FMeshElement& Element, int BufIdx )
{
  if ( !Element.Texture )
  {
    GLogf(LOG_ERR, "Null texture");
    return;
  }

  // Switch to this element's desired shader
  FGLShaderProgram& Program = SetPolygonEffects( Element.Texture, Element.VertexShader, Element.FragmentShader, Element.PolyFlags );

  // Construct MVP matrix
  FMatrix4x4 Tmp;
  Element.Rotation.GetMatrix( Tmp );

  FMatrix4x4 MVP;
  Element.Scale.GetScaleMatrix( MVP );
  MVP *= Tmp;

  Element.Translation.GetTranslationMatrix( Tmp );
  MVP *= Tmp;
  MVP *= ViewMatrix;
  MVP *= CurrentViewport->Perspective;

  glUniformMatrix4fv( Program.VUniformMVP, 1, false, (GLfloat*)MVP.Data );

  // Push vertices into a buffer
  glBindBuffer( GL_ARRAY_BUFFER, VertexBuffers[BufIdx] );
  glBufferData( GL_ARRAY_BUFFER, Element.Vertices.Size() * sizeof( FVertexUV ), Element.Vertices.Data(), GL_STATIC_DRAW );
  glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof( GLfloat ), 0 );
  glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof( GLfloat ), (GLvoid*)(3 * sizeof( GLfloat )) );
  glEnableVertexAttribArray( 0 );
  glEnableVertexAttribArray( 1 );

  // Push indices into a buffer
  glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, IndexBuffers[BufIdx] );
  glBufferData( GL_ELEMENT_ARRAY_BUFFER, Element.Indices.Size() * sizeof( u32 ), Element.Indices.Data(), GL_STATIC_DRAW );

  // Draw element
  glDrawElements( GL_TRIANGLES, Element.Indices.Size(), GL_UNSIGNED_INT, 0 );

  // Remove element from queue
  glDisableVertexAttribArray( 0 );
  glDisableVertexAttribArray( 1 );
}

/*-----------------------------------------------------------------------------
 * Render Device Level-of-Detail mesh rendering
 * Renders a frame of a LOD mesh to the screen
-----------------------------------------------------------------------------*/
void UOpenGL3RenderDevice::RenderLodMesh( FMeshElement& Element, int BufIdx )
{
  if ( !Element.Texture )
  {
    GLogf(LOG_ERR, "Null texture");
    return;
  }

  // Handle current polygon effects
  FGLShaderProgram& Program = SetPolygonEffects( Element.Texture, Element.VertexShader, Element.FragmentShader, Element.PolyFlags );

  // Construct MVP matrix
  FMatrix4x4 Tmp;
  Element.Rotation.GetMatrix( Tmp );

  FMatrix4x4 MVP;
  Element.Scale.GetScaleMatrix( MVP );
  MVP *= Tmp;

  Element.Translation.GetTranslationMatrix( Tmp );
  MVP *= Tmp;
  MVP *= ViewMatrix;
  MVP *= CurrentViewport->Perspective;

  glUniformMatrix4fv( Program.VUniformMVP, 1, false, (GLfloat*)MVP.Data );

  // Push vertices into a buffer
  glBindBuffer( GL_ARRAY_BUFFER, VertexBuffers[BufIdx] );
  glBufferData( GL_ARRAY_BUFFER, Element.Vertices.Size() * sizeof( FVertexUV ), Element.Vertices.Data(), GL_STATIC_DRAW );
  glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof( GLfloat ), 0 );
  glVertexAttribPointer( 1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof( GLfloat ), (GLvoid*)(3 * sizeof( GLfloat )) );
  glEnableVertexAttribArray( 0 );
  glEnableVertexAttribArray( 1 );

  // Draw element
  glDrawArrays( GL_TRIANGLES, 0, Element.Vertices.Size() );

  // Remove element from queue
  glDisableVertexAttribArray( 0 );
  glDisableVertexAttribArray( 1 );
}

/*-----------------------------------------------------------------------------
 * Render Device DrawText
 *
 * Simply initializes a set of FGLVertex instances to describe where this
 * text will be drawn in the canvas draw step. This is supposed to be more
 * optimized than calling DrawTile per letter, since one batch of vertices
 * goes out per text string
 -----------------------------------------------------------------------------*/
void UOpenGL3RenderDevice::DrawText( UFont* Font, FBoxInt2D& Dim, FString& Text, int PolyFlags )
{
  // Don't draw things that won't show up anyway
  if ( Font == NULL || Dim.IsZero() || Text.Size() == 0 )
    return;
  
  FVertexUV CharVerts[4];

  FCanvasElement TextElement;
  UFont::FFontTexture& FontTexture = Font->FontTextures->At( 0 );
  TextElement.Type = ELEM_Tile;
  TextElement.Texture = FontTexture.Texture;
  TextElement.VertexShader = SHADER_VERTEX_DEFAULT;
  TextElement.FragmentShader = SHADER_FRAG_DEFAULT;
  TextElement.PolyFlags = PolyFlags;

  int CurX = Dim.X;
  int CurY = Dim.Y;
  int EndX = 0;
  int EndY = 0;
  int WrapX = Dim.Width;

  // Go through each letter
  for ( int i = 0; i < Text.Size(); i++ )
  {
    UFont::FFontCharInfo CharInfo = FontTexture.Characters->At( Text[i] );
    
    // Make sure invalid characters show up as something that looks broken
    if ( CharInfo.Width == 0 || CharInfo.Height == 0 )
      CharInfo = FontTexture.Characters->At( 0x7f );

    // Wrap text if necessary
    if ( CurX + CharInfo.Width > WrapX )
      CurY += CharInfo.Height;
    if ( CurY > CurrentViewport->Viewport->Height )
      return; // We can't see more text from here

    // Set end coordinates
    EndX = CurX + CharInfo.Width;
    EndY = CurY + CharInfo.Height;

    // Calculate vertex coordinates for this character
    CharVerts[0].X = CurX;
    CharVerts[0].Y = CurY;
    CharVerts[0].Z = 0.0f;
    CharVerts[0].U = (float)CharInfo.X / (float)TextElement.Texture->USize;
    CharVerts[0].V = (float)CharInfo.Y / (float)TextElement.Texture->VSize;

    CharVerts[1].X = EndX;
    CharVerts[1].Y = CurY;
    CharVerts[1].Z = 0.0f;
    CharVerts[1].U = (float)(CharInfo.X + CharInfo.Width) / (float)TextElement.Texture->USize;
    CharVerts[1].V = (float)CharInfo.Y / (float)TextElement.Texture->VSize;

    CharVerts[2].X = CurX;
    CharVerts[2].Y = EndY;
    CharVerts[2].Z = 0.0f;
    CharVerts[2].U = (float)(CharInfo.X) / (float)TextElement.Texture->USize;
    CharVerts[2].V = (float)(CharInfo.Y + CharInfo.Height) / (float)TextElement.Texture->VSize;

    CharVerts[3].X = EndX;
    CharVerts[3].Y = EndY;
    CharVerts[3].Z = 0.0f;
    CharVerts[3].U = (float)(CharInfo.X + CharInfo.Width) / (float)TextElement.Texture->USize;
    CharVerts[3].V = (float)(CharInfo.Y + CharInfo.Height) / (float)TextElement.Texture->VSize;

    // Adjust indices
    int Offset = i * 4;
    u32 Indices[6] =
    {
      2+Offset, 1+Offset, 0+Offset,
      1+Offset, 2+Offset, 3+Offset
    };

    // Push vertices in
    TextElement.Vertices.Append( CharVerts, 4 );
    TextElement.Indices.Append( Indices, 6 );

    // Update coordinates
    CurX += CharInfo.Width;
  }

  CanvasDrawList.Push( TextElement );
  NumBuffers++;
}

/*-----------------------------------------------------------------------------
 * Render Device DrawTile
 *
 * Simply initializes a set of FGLVertex instances to describe where this
 * tile will be drawn in the canvas draw step
 -----------------------------------------------------------------------------*/
void UOpenGL3RenderDevice::DrawTile( UTexture* Tex, FBoxInt2D& Dim, FRotator& Rot, float U, float V, float UL, float VL, int PolyFlags )
{
  // Don't draw things that won't show up anyway
  if ( Tex == NULL || (Dim.IsZero()) )
    return;

  FCanvasElement NewElement;
  NewElement.Type = ELEM_Tile;

  // Figure out vertex coordinates
  // Here, Min = top left, and Max = bottom right
  FVertexUV Coords[4];

  Coords[0].X = Dim.X;
  Coords[0].Y = Dim.Y;
  Coords[0].Z = 0.0f;
  Coords[0].U = U;
  Coords[0].V = V;

  Coords[1].X = Dim.Width;
  Coords[1].Y = Dim.Y;
  Coords[1].Z = 0.0f;
  Coords[1].U = UL;
  Coords[1].V = V;

  Coords[2].X = Dim.X;
  Coords[2].Y = Dim.Height;
  Coords[2].Z = 0.0f;
  Coords[2].U = U;
  Coords[2].V = VL;

  Coords[3].X = Dim.Width;
  Coords[3].Y = Dim.Height;
  Coords[3].Z = 0.0f;
  Coords[3].U = UL;
  Coords[3].V = VL;

  // Set up indices
  u32 Indices[6] =
  {
    2, 1, 0,
    1, 2, 3
  };

  // Set up canvas element
  NewElement.Vertices.Append( Coords, 4 );
  NewElement.Indices.Append( Indices, 6 );
  NewElement.Texture = Tex;
  NewElement.VertexShader = SHADER_VERTEX_DEFAULT;
  NewElement.FragmentShader = SHADER_FRAG_DEFAULT;
  NewElement.PolyFlags = PolyFlags;

  // Push to queue
  CanvasDrawList.Push( NewElement );
  NumBuffers++;
}

/*-----------------------------------------------------------------------------
 * Render Device DrawMesh
 *
 * Simply initializes a set of FGLVertex instances to describe where this
 * mesh will be drawn in the canvas draw step
 -----------------------------------------------------------------------------*/
void UOpenGL3RenderDevice::DrawMesh( UMesh* Mesh, FMeshAnimSeq& AnimSeq, float AnimFrame, FVector& Loc, FVector& Scale, FRotator& Rot, int PolyFlags )
{
  // Figure out which frames we're in between and 
  float FrameNum = AnimSeq.NumFrames * AnimFrame;
  u32 BackFrame = floor( FrameNum );
  float Weight = FrameNum - BackFrame;
  u32 FrameOffset = (MIN( BackFrame, AnimSeq.NumFrames - 1 ) * Mesh->FrameVerts);
  u32 FrameOffsetOne = (MIN( (BackFrame + 1), AnimSeq.NumFrames - 1) * Mesh->FrameVerts);

  // Vertex meshes
  if ( Mesh->Class == ULodMesh::StaticClass() )
  {
    ULodMesh* LodMesh = (ULodMesh*)Mesh;

    // Set up different mesh elements that have different materials
    // +1 for mysterious weapon triangle??? Check Botpack.FCommando
    FMeshElement* Elements = new FMeshElement[LodMesh->Materials.Size()+1];

    for ( int i = 0; i < LodMesh->Faces.Size(); i++ )
    {
      FLodFace& Face = LodMesh->Faces[i];
      FLodWedge Wedge[3] =
      {
        LodMesh->Wedges[Face.WedgeIndex[0]],
        LodMesh->Wedges[Face.WedgeIndex[1]],
        LodMesh->Wedges[Face.WedgeIndex[2]]
      };
      int ElementIndex = Face.MaterialIndex;

      FVertexUV Vertex[3];

      Vertex[0].X = -Lerp( LodMesh->Verts[Wedge[2].VertexIndex + FrameOffset].X, LodMesh->Verts[Wedge[2].VertexIndex + FrameOffsetOne].X, Weight );
      Vertex[0].Y = -Lerp( LodMesh->Verts[Wedge[2].VertexIndex + FrameOffset].Y, LodMesh->Verts[Wedge[2].VertexIndex + FrameOffsetOne].Y, Weight );
      Vertex[0].Z = -Lerp( LodMesh->Verts[Wedge[2].VertexIndex + FrameOffset].Z, LodMesh->Verts[Wedge[2].VertexIndex + FrameOffsetOne].Z, Weight );
      Vertex[0].U = Wedge[2].S / 255.0;
      Vertex[0].V = Wedge[2].T / 255.0;

      Vertex[1].X = -Lerp( LodMesh->Verts[Wedge[1].VertexIndex + FrameOffset].X, LodMesh->Verts[Wedge[1].VertexIndex + FrameOffsetOne].X, Weight );
      Vertex[1].Y = -Lerp( LodMesh->Verts[Wedge[1].VertexIndex + FrameOffset].Y, LodMesh->Verts[Wedge[1].VertexIndex + FrameOffsetOne].Y, Weight );
      Vertex[1].Z = -Lerp( LodMesh->Verts[Wedge[1].VertexIndex + FrameOffset].Z, LodMesh->Verts[Wedge[1].VertexIndex + FrameOffsetOne].Z, Weight );
      Vertex[1].U = Wedge[1].S / 255.0;
      Vertex[1].V = Wedge[1].T / 255.0;

      Vertex[2].X = -Lerp( LodMesh->Verts[Wedge[0].VertexIndex + FrameOffset].X, LodMesh->Verts[Wedge[0].VertexIndex + FrameOffsetOne].X, Weight );
      Vertex[2].Y = -Lerp( LodMesh->Verts[Wedge[0].VertexIndex + FrameOffset].Y, LodMesh->Verts[Wedge[0].VertexIndex + FrameOffsetOne].Y, Weight );
      Vertex[2].Z = -Lerp( LodMesh->Verts[Wedge[0].VertexIndex + FrameOffset].Z, LodMesh->Verts[Wedge[0].VertexIndex + FrameOffsetOne].Z, Weight );
      Vertex[2].U = Wedge[0].S / 255.0;
      Vertex[2].V = Wedge[0].T / 255.0;

      Elements[ElementIndex].Vertices.Append( Vertex, 3 );
    }

    for ( int i = 0; i < LodMesh->Materials.Size(); i++ )
    {
      if ( Elements[i].Vertices.Size() > 0 )
      {
        Elements[i].Type = MESH_Vertex;
        Elements[i].VertexShader = SHADER_VERTEX_DEFAULT;
        Elements[i].FragmentShader = SHADER_FRAG_DEFAULT;
        Elements[i].PolyFlags = LodMesh->Materials[i].Flags; // TODO: Factor global polyflags in
        Elements[i].Texture = LodMesh->Textures[LodMesh->Materials[i].TextureIndex];
        Elements[i].AnimFrame = AnimFrame;
        Elements[i].Translation = Loc;
        Elements[i].Scale = Scale;
        Elements[i].Rotation = Rot;
        MeshDrawList.Push( Elements[i] );
        NumBuffers++;
      }
    }

    delete[] Elements;
  }
}

// TODO:
void UOpenGL3RenderDevice::DrawGrid( FBox& Dim, FColor& Color )
{
}

/*-----------------------------------------------------------------------------
 * Render Device DrawCube
 *
 * Draws a simple cube to test basic mesh rendering.
 * If this doesn't work, no other form of 3D rendering will
 -----------------------------------------------------------------------------*/
void UOpenGL3RenderDevice::DrawCube( FVector& Loc, FRotator& Rot, FVector& Scale, UTexture* Tex )
{
  FMeshElement Element;
  Element.Type = MESH_Primitive;
  Element.AnimFrame = 0.f;
  Element.AnimVerts = 0;

  FVertexUV Verts[8];

  *(FVector*)&Verts[0] = FVector(  1.0,  1.0, -1.0 );
  Verts[0].U = 0;
  Verts[0].V = 1;

  *(FVector*)&Verts[1] = FVector(  1.0, -1.0, -1.0 );
  Verts[1].U = 1;
  Verts[1].V = 1;

  *(FVector*)&Verts[2] = FVector(  1.0,  1.0,  1.0 );
  Verts[2].U = 0;
  Verts[2].V = 1;

  *(FVector*)&Verts[3] = FVector(  1.0, -1.0,  1.0 );
  Verts[3].U = 1;
  Verts[3].V = 1;

  *(FVector*)&Verts[4] = FVector( -1.0,  1.0, -1.0 );
  Verts[4].U = 0;
  Verts[4].V = 0;

  *(FVector*)&Verts[5] = FVector( -1.0, -1.0, -1.0 );
  Verts[5].U = 1;
  Verts[5].V = 0;

  *(FVector*)&Verts[6] = FVector( -1.0,  1.0,  1.0 );
  Verts[6].U = 0;
  Verts[6].V = 0;

  *(FVector*)&Verts[7] = FVector( -1.0, -1.0,  1.0 );
  Verts[7].U = 1;
  Verts[7].V = 0;

  GLuint Indices[12 * 3] =
  {
    4, 2, 0,
    2, 7, 3,
    6, 5, 7,
    1, 7, 5,
    0, 3, 1,
    4, 1, 5,
    4, 6, 2,
    2, 6, 7,
    6, 4, 5,
    1, 3, 7,
    0, 2, 3,
    4, 0, 1
  };

  Element.Vertices.Append( Verts, 8 );
  Element.Indices.Append( Indices, 12*3 );
  Element.Texture = Tex;
  Element.VertexShader = SHADER_VERTEX_DEFAULT;
  Element.FragmentShader = SHADER_FRAG_DEFAULT;
  Element.AnimFrame = 0.f;
  Element.Scale = Scale;
  Element.Rotation = Rot;
  Element.Translation = Loc;

  MeshDrawList.Push( Element );
  NumBuffers++;
}

/*-----------------------------------------------------------------------------
 * Render Device Polygon Effects
 *
 * Handles polygon effects by manipulating GL state to achieve desired effect
-----------------------------------------------------------------------------*/
FGLShaderProgram& UOpenGL3RenderDevice::SetPolygonEffects( UTexture* Texture, int VertexShader, int FragmentShader, int PolyFlags )
{
  // Register texture if needed
  if ( !Texture->AnimCurrent->TextureHandle )
    RegisterTexture( Texture->AnimCurrent );
  else if ( Texture->bRealtimeChanged )
    UpdateTexture( Texture );

  if ( Texture->bMasked )
    PolyFlags |= PF_Masked;

  // Switch to the desired shader
  if ( FragmentShader == SHADER_FRAG_DEFAULT && Texture->DetailTexture != NULL )
    FragmentShader = SHADER_FRAG_DETAIL;

  FGLShaderProgram& Program = ShaderManager.GetProgram( VertexShader, FragmentShader );
  Program.Use();
  glUniform1i( Program.FUniformTex, 0 );
  glUniform1i( Program.FUniformDetailTex, 1 );

  // Handle polyflag state
  // Two sided polys
  if ( PolyFlags & PF_TwoSided )
    glDisable( GL_CULL_FACE );
  else
    glEnable( GL_CULL_FACE );

  if ( PolyFlags & PF_Translucent )
    glBlendFunc( GL_ONE, GL_ONE_MINUS_SRC_COLOR );
  else
    glBlendFunc( GL_ONE, GL_ZERO );

  // Masked polygons
  glUniform1i( Program.FUniformMasked, !!(PolyFlags & PF_Masked) );
  
  // Handle texture binding
  glActiveTexture( GL_TEXTURE0 );
  glBindTexture( GL_TEXTURE_2D, VOIDP_TO_GLUINT(Texture->AnimCurrent->TextureHandle) );

  // Handle detail texture
  glActiveTexture( GL_TEXTURE1 );
  if ( FragmentShader == SHADER_FRAG_DETAIL && Texture->DetailTexture != NULL )
    glBindTexture( GL_TEXTURE_2D, VOIDP_TO_GLUINT(Texture->DetailTexture->TextureHandle) );
  else
    glBindTexture( GL_TEXTURE_2D, 0 );

  // Return the selected shader program
  return Program;
}

/*-----------------------------------------------------------------------------
 * Render Device Texture Registration
 *
 * The general order of operations is as follows
 * - Create a new texture handle
 * - Bind the new texture handle
 * - Set texture filtering based on texture properties
 * - Set texture wrap setting based on texture properties
 * - Prepare the texture for upload to the graphics device
 * - Send the texture to the graphics device
 * - Free any leftover memory from texture preparation
 * - Set the texture's handle to our newly made GL handle
 *
 * For standard "P8" textures, do the following
 * - Create a buffer that fits each channel for each pixel
 * - Iterate through each pixel
 * - Grab the palette index for that pixel
 * - Unpack each color channel into our created buffer
 * - Buffer is ready for device upload
 *
 * TODO: Check render options for disabling all texture filtering
-----------------------------------------------------------------------------*/
bool UOpenGL3RenderDevice::RegisterTexture( UTexture* Texture )
{
  if ( Texture->Class == UWetTexture::StaticClass() )
    return false; // not yet

  // Create texture handle
  GLuint TextureHandle;
  glGenTextures( 1, &TextureHandle );
  glBindTexture( GL_TEXTURE_2D, TextureHandle );
  
  // Set texture filtering
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (Texture->bNoSmooth) ? GL_NEAREST : GL_LINEAR );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (Texture->bNoSmooth) ? GL_NEAREST : GL_LINEAR );

  // Set texture wrapping
  GLenum UClampMode;
  GLenum VClampMode;

  switch ( Texture->UClampMode )
  {
    case UWrap:
      UClampMode = GL_REPEAT;
      break;
    case UClamp:
      UClampMode = GL_CLAMP_TO_EDGE;
      break;
  }

  switch ( Texture->VClampMode )
  {
  case VWrap:
    VClampMode = GL_REPEAT;
    break;
  case UClamp:
    VClampMode = GL_CLAMP_TO_EDGE;
    break;
  }

  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, UClampMode );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, VClampMode );

  // TODO: Any other parameters that need to be set?

  // Unpack the texture to a buffer
  u8* TexBuf = Texture->Mips[0].DataArray.Data();
  u32* UnpackBuf = new u32[Texture->USize * Texture->VSize];
  u32* UnpackPtr = UnpackBuf;

  FColor* Palette = Texture->Palette->Colors;

  for ( int i = 0; i < Texture->VSize; i++ )
  {
    for ( int j = 0; j < Texture->USize; j++ )
    {
      u8 PalIdx = TexBuf[(i * Texture->USize) + j];

      if ( Texture->bMasked && PalIdx == 0 )
        *UnpackPtr++ = 0;
      else
        *UnpackPtr++ = 0xFF000000 | Palette[PalIdx].Data;
    }
  }

  // TODO: Mipmapping
  // Buffer texture data
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, Texture->USize, Texture->VSize, 0, GL_RGBA, GL_UNSIGNED_BYTE, UnpackBuf );

  // Free temporary buffer
  delete[] UnpackBuf;

  // Set texture's texture handle
  Texture->TextureHandle = GLUINT_TO_VOIDP(TextureHandle);

  // Register detail texture if needed
  if ( Texture->DetailTexture && !Texture->DetailTexture->TextureHandle )
    RegisterTexture( Texture->DetailTexture );

  return true;
}

/*-----------------------------------------------------------------------------
 * Render Device Texture Update
-----------------------------------------------------------------------------*/
bool UOpenGL3RenderDevice::UpdateTexture( UTexture* Texture )
{
  if ( !Texture->TextureHandle )
    return false;

  // Set texture handle
  glBindTexture( GL_TEXTURE_2D, VOIDP_TO_GLUINT(Texture->TextureHandle) );

  // Unpack the texture to a buffer
  u8* TexBuf = Texture->Mips[0].DataArray.Data();

  u32* UnpackBuf = new u32[Texture->USize * Texture->VSize];
  memset( UnpackBuf, 0, Texture->USize * Texture->VSize );

  u32* UnpackPtr = UnpackBuf;

  FColor* Palette = Texture->Palette->Colors;

  for ( int i = 0; i < Texture->VSize; i++ )
  {
    for ( int j = 0; j < Texture->USize; j++ )
    {
      u8 PalIdx = TexBuf[(i * Texture->USize) + j];

      if ( Texture->bMasked && PalIdx == 0 )
        *UnpackPtr++ = 0;
      else
        *UnpackPtr++ = 0xFF000000 | Palette[PalIdx].Data;
    }
  }

  // TODO: Mipmapping
  // Buffer texture data
  glTexSubImage2D( GL_TEXTURE_2D, 0, 0, 0, Texture->USize, Texture->VSize, GL_RGBA, GL_UNSIGNED_BYTE, UnpackBuf );

  // Free temporary buffer
  delete[] UnpackBuf;

  // Unset bRealtimeChanged
  Texture->bRealtimeChanged = false;

  return true;
}

/*-----------------------------------------------------------------------------
 * Render Device Texture Removal
 *
 * Simply check if the texture has a valid handle, and delete it if so
-----------------------------------------------------------------------------*/
bool UOpenGL3RenderDevice::UnregisterTexture( UTexture* Texture )
{
  if ( Texture->TextureHandle )
  {
    GLuint TextureHandle = VOIDP_TO_GLUINT(Texture->TextureHandle);
    glDeleteTextures( 1, &TextureHandle );
  }

  return true;
}

/*-----------------------------------------------------------------------------
 * Render Device Shader Compilation
 *
 * Compile our set of frequently used shaders for normal rendering
 * This list includes the following
 * - A default shader (CURRENTLY IN TESTING)
-----------------------------------------------------------------------------*/
bool UOpenGL3RenderDevice::CompileShaderPrograms()
{
  int rc;
  // Go to libunr root directory
  char* Tmp = getcwd( NULL, 0 );

#if defined LIBUNR_POSIX
  std::string HomeConfig = GSystem->GetHomeLibunrDir();
  rc = chdir( HomeConfig.c_str() );

  if (rc < 0)
  {
    GLogf(LOG_ERR, "Failed to change to config directory \'%s\'", HomeConfig.c_str());
    return false;
  }

  if ( !GSystem->FileExists( "OpenGL3" ) )
  {
    // Copy shaders from install prefix
    if ( !GSystem->MakeDir( "OpenGL3" ) )
    {
      GLogf( LOG_ERR, "Failed to make OpenGL 3.x shader folder in home config directory" );
      return false;
    }

    GSystem->CopyFile( INSTALL_PREFIX "/share/libunr/modules/OpenGL3/DefaultShader.vert", "OpenGL3/DefaultShader.vert" );
    GSystem->CopyFile( INSTALL_PREFIX "/share/libunr/modules/OpenGL3/DefaultShader.frag", "OpenGL3/DefaultShader.frag" );
    GSystem->CopyFile( INSTALL_PREFIX "/share/libunr/modules/OpenGL3/DetailTexShader.frag", "OpenGL3/DetailTexShader.frag" );
  }
#else
  chdir( GSystem->LibunrPath );
#endif

  FStringFilePath DefVertPath( "OpenGL3", "DefaultShader", "vert" );
  FStringFilePath DefFragPath( "OpenGL3", "DefaultShader", "frag" );
  FStringFilePath DetailPath( "OpenGL3", "DetailTexShader", "frag" );
  FStringFilePath TranslucentPath( "OpenGL3", "TranslucentShader", "frag" );

  // Build standalone shader files
  if ( !ShaderManager.AddVertexShader( DefVertPath ) )
    return false;
  if ( !ShaderManager.AddFragmentShader( DefFragPath ) )
    return false;
  if ( !ShaderManager.AddFragmentShader( DetailPath ) )
    return false;
  if ( !ShaderManager.AddFragmentShader( TranslucentPath ) )
    return false;

  // Compile shader programs
  if ( !ShaderManager.CompileAllShaders() )
    return false;

  // Restore last directory and cleanup
  chdir( Tmp );
  free( Tmp );

  return true;
}

/*-----------------------------------------------------------------------------
 * FCanvasElement
-----------------------------------------------------------------------------*/
FCanvasElement::FCanvasElement()
{
}

FCanvasElement::FCanvasElement( const FCanvasElement& E )
{
  Type = E.Type;
  Texture = E.Texture;
  VertexShader = E.VertexShader;
  FragmentShader = E.FragmentShader;
  PolyFlags = E.PolyFlags;
  Vertices = E.Vertices;
  Indices = E.Indices;
  Mesh = E.Mesh;
  AnimFrame = E.AnimFrame;
  AnimVerts = E.AnimVerts;
  Scale = E.Scale;
  Rotation = E.Rotation;
  Translation = E.Translation;
}

FCanvasElement::~FCanvasElement()
{
  Vertices.Clear();
  Vertices.Reserve( 0 );

  Indices.Clear();
  Indices.Reserve( 0 );
}

/*-----------------------------------------------------------------------------
 * FMeshElement Destructor
-----------------------------------------------------------------------------*/
FMeshElement::FMeshElement()
{
}

FMeshElement::FMeshElement( const FMeshElement& E )
{
  Type = E.Type;
  Texture = E.Texture;
  VertexShader = E.VertexShader;
  FragmentShader = E.FragmentShader;
  PolyFlags = E.PolyFlags;
  AnimFrame = E.AnimFrame;
  AnimVerts = E.AnimVerts;
  Scale = E.Scale;
  Rotation = E.Rotation;
  Translation = E.Translation;
  Vertices = E.Vertices;
  Indices = E.Indices;
}

FMeshElement::~FMeshElement()
{
  Vertices.Clear();
  Vertices.Reclaim();
}

IMPLEMENT_MODULE_CLASS( UOpenGL3RenderDevice );
