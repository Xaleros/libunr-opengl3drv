#version 330 core

uniform mat4 MVP;

layout (location = 0) in vec3 Position;
layout (location = 1) in vec2 UVs;

out vec2 UVOut;

void main()
{
	gl_Position = MVP * vec4( Position.x, Position.y, Position.z, 1.0 );
	UVOut = UVs;
}
