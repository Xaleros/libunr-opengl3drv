#version 330 core

in vec2 UVOut;
out vec4 FinalColor;

uniform sampler2D Tex;        // The standard texture
uniform sampler2D DetailTex;  // A detail texture to blend in if the fragment is close enough
uniform bool bMasked;         // Mask out black (0,0,0) pixels

void main()
{
	vec4 Black  = vec4(0.0, 0.0, 0.0, 1.0);
	vec4 Color  = texture(Tex, UVOut);
	vec4 Detail = texture(DetailTex, UVOut.st * 8.0);

	// Masked textures
	if ( Color.a < 0.5 )
		discard;

	// Masked polygons (textures that are not masked but the surface is)
	if ( bMasked && Color == Black )
		discard;

	float dist = gl_FragCoord.z / gl_FragCoord.w;
	if ( dist < 2048.0 )
	{
		if ( Color.r > 0.5 && Color.g > 0.5 && Color.b > 0.5 )
			FinalColor = (1 - (1-2*(Color-0.5)) * (1-Detail));
		else
			FinalColor = 2*Color*Detail;
	}
	else
		FinalColor = Color;
}
