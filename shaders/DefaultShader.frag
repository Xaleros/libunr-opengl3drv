#version 330 core

in vec2 UVOut;
out vec4 FinalColor;

uniform sampler2D Tex;        // The standard texture
uniform bool bMasked;         // Mask out black (0,0,0) pixels

void main()
{
	vec4 Black  = vec4(0.0, 0.0, 0.0, 1.0);
	vec4 Color  = texture(Tex, UVOut);

	// Masked textures
	if ( Color.a < 0.5 )
		discard;

	// Masked polygons (textures that are not masked but the surface is)
	if ( bMasked && Color == Black )
		discard;

	FinalColor = Color;
}
