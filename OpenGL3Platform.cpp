#include "OpenGL3RenderDevice.h"
#include "OpenGL3Platform.h"

#if defined LIBUNR_WIN32

class WindowsFakeContext
{
public:
	PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB = nullptr;
	PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB = nullptr;

	bool IsInitialized()
	{
		if (wglChoosePixelFormatARB == nullptr)
		{
			return false;
		}

		if (wglCreateContextAttribsARB == nullptr)
		{
			return false;
		}

		return true;
	}

	bool Init()
	{
		if (IsInitialized())
		{
			return true;
		}

		// Setup a fake window to gain access to extensions
		HINSTANCE hInst = GetModuleHandle( NULL );
		memset( &FakeCls, 0, sizeof( FakeCls ) );

		FakeCls.cbSize = sizeof( FakeCls );
		FakeCls.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
		FakeCls.lpfnWndProc = DefWindowProc; // We are not actually doing anything with the window here
		FakeCls.hInstance = hInst;
		FakeCls.hCursor = LoadCursor( NULL, IDC_ARROW );
		FakeCls.lpszClassName = "FakeGL3WndClass";

		if ( !RegisterClassEx( &FakeCls ) )
		{
			GLogf( LOG_CRIT, "Failed to create fake window class for GL 3.x context" );
			return false;
		}

		FakeWnd = CreateWindow( "FakeGL3WndClass", "FakeGL3Window", WS_CLIPSIBLINGS | WS_CLIPCHILDREN, 0, 0, 1, 1, NULL, NULL, hInst, NULL );
		if ( !FakeWnd )
		{
			GLogf( LOG_CRIT, "Failed to create fake window for GL 3.x context" );
			return false;
		}

		// Create fake GL context
		FakeDC = GetDC( FakeWnd );
		memset( &Pfd, 0, sizeof( Pfd ) );

		Pfd.nSize = sizeof( Pfd );
		Pfd.nVersion = 1;
		Pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		Pfd.iPixelType = PFD_TYPE_RGBA;
		Pfd.cColorBits = 32;
		Pfd.cAlphaBits = 8;
		Pfd.cDepthBits = 24;

		int PfdId = ChoosePixelFormat( FakeDC, &Pfd );
		if ( PfdId == 0 )
		{
			GLogf( LOG_CRIT, "Failed to get fake pixel format for GL 3.x context" );
			return false;
		}

		if ( !SetPixelFormat( FakeDC, PfdId, &Pfd ) )
		{
			GLogf( LOG_CRIT, "Failed to set fake pixel format for GL 3.x context" );
			return false;
		}

		FakeRC = wglCreateContext( FakeDC );
		if ( FakeRC == 0 )
		{
			GLogf( LOG_CRIT, "Failed to create fake GL context" );
			return false;
		}

		if ( !wglMakeCurrent( FakeDC, FakeRC ) )
		{
			GLogf( LOG_CRIT, "Failed to make fake GL context the current context" );
			return false;
		}

		wglChoosePixelFormatARB = (PFNWGLCHOOSEPIXELFORMATARBPROC)wglGetProcAddress("wglChoosePixelFormatARB");
		wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");

		return IsInitialized();
	}

protected:
	PIXELFORMATDESCRIPTOR Pfd;
	HGLRC FakeRC;
	HDC FakeDC;
	HWND FakeWnd;
	WNDCLASSEX FakeCls;
};

class WindowsGlPlatformContext: public GlPlatformContext
{
public:
	typedef WindowsGlPlatformDriver::SharedContext SharedContext;

	WindowsGlPlatformContext(UWindowsViewport * viewport,
	                         HDC deviceContext,
	                         SharedContext & sharedContext):
		deviceContext(deviceContext),
		renderingContext(**sharedContext),
		viewport(viewport),
		sharedContext(sharedContext)
	{}

	bool LoadExtensions() override
	{
		return LoadExtensionsImpl(GetProcAddress);
	}

	void Swap() override
	{
		SwapBuffers(viewport->DrawContext);
	}

	bool MakeCurrent() override
	{
		return wglMakeCurrent(deviceContext, renderingContext) == TRUE;
	}

	virtual ~WindowsGlPlatformContext()
	{
		wglMakeCurrent( NULL, NULL );

		ReleaseDC( viewport->Window, deviceContext );
	}
protected:
	static void * GetProcAddress(const char * procName)
	{
		return reinterpret_cast<void *>(wglGetProcAddress(procName));
	}

	SharedContext sharedContext;

	HDC deviceContext;
	HGLRC renderingContext;
	UWindowsViewport * viewport;
};

WindowsFakeContext windowsFakeContext;

WindowsGlPlatformContext * WindowsGlPlatformDriver::CreateMainContext(UOpenGL3RenderDevice * device, UWindowsViewport * viewport, SharedContext & sharedContext)
{
	if (!windowsFakeContext.Init())
	{
		GLogf( LOG_ERR, "Failed to create fake context" );
		return nullptr;
	}

	HDC DC = GetDC(viewport->Window);

	// TODO: Modify attribs based on config
	const int PixelAttribs[] =
	{
		WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
		WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
		WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
		WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
		WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
		WGL_COLOR_BITS_ARB, 32,
		WGL_ALPHA_BITS_ARB, 8,
		WGL_DEPTH_BITS_ARB, 24,
		WGL_STENCIL_BITS_ARB, 8,
		0
	};

	int PfdId;
	u32 NumFormats;

	// Get our pixel format
	bool Status = windowsFakeContext.wglChoosePixelFormatARB(DC, PixelAttribs, NULL, 1, &PfdId, &NumFormats );
	if ( !Status || !NumFormats )
	{
		GLogf( LOG_ERR, "No valid pixel format found, can't create OpenGL context" );
		return nullptr;
	}

	PIXELFORMATDESCRIPTOR Pfd;
	DescribePixelFormat( DC, PfdId, sizeof( Pfd ), &Pfd );
	SetPixelFormat( DC, PfdId, &Pfd );

	// Specify an OpenGL 3.3 context
	// All hardware that is 3.x compliant afaik can do 3.3
	int ContextAttribs[] =
	{
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
		WGL_CONTEXT_MINOR_VERSION_ARB, 3,
		WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
		0
	};

	// Check if this context type is supported
	HGLRC RC = windowsFakeContext.wglCreateContextAttribsARB( DC, 0, ContextAttribs );
	if ( !RC )
	{
		GLogf( LOG_ERR, "Failed to create OpenGL 3.3 context" );
		return nullptr;
	}

	sharedContext = std::make_shared<WindowsRawContext>(RC);

	return new WindowsGlPlatformContext(viewport, DC, sharedContext);
}

WindowsGlPlatformContext * WindowsGlPlatformDriver::CreateSubordinateContext(UOpenGL3RenderDevice * device, UWindowsViewport * viewport, SharedContext & sharedContext)
{
	HDC DC = GetDC(viewport->Window);

	return new WindowsGlPlatformContext(viewport, DC, sharedContext);
}

WindowsGlPlatformDriver windowsDriver;
#endif /* LIBUNR_WIN32 */

#if defined LIBUNR_SDL2
class Sdl2GlPlatformContext : public GlPlatformContext
{
public:
	using SharedContext = Sdl2GlPlatformDriver::SharedContext;

	Sdl2GlPlatformContext(USdlViewport * viewport, SharedContext & context):
		viewport(viewport),
		glContext(**context),
		context(context)
	{}

	~Sdl2GlPlatformContext() {}

	bool LoadExtensions() override
	{
		return LoadExtensionsImpl(SDL_GL_GetProcAddress);
	}

	void Swap() override
	{
		SDL_GL_SwapWindow(viewport->getWindow());
	}

	bool MakeCurrent() override
	{
		return SDL_GL_MakeCurrent(viewport->getWindow(), glContext) != 0;
	}
protected:
	USdlViewport * viewport;
	SDL_GLContext glContext;
	SharedContext context;
};

Sdl2GlPlatformContext * Sdl2GlPlatformDriver::CreateMainContext(UOpenGL3RenderDevice * device, USdlViewport * viewport, SharedContext & sharedContext)
{
	viewport->Show();

	auto contextData = SDL_GL_CreateContext(viewport->getWindow());
	if (!contextData)
	{
		GLogf(LOG_CRIT, "Failed to construct SDL2 OpenGL context");
		return nullptr;
	}

	sharedContext = std::make_shared<Sdl2RawContext>(contextData);

	return new Sdl2GlPlatformContext(viewport, sharedContext);
}

Sdl2GlPlatformContext * Sdl2GlPlatformDriver::CreateSubordinateContext(UOpenGL3RenderDevice * device, USdlViewport * viewport, SharedContext & sharedContext)
{
	return new Sdl2GlPlatformContext(viewport, sharedContext);
}

Sdl2GlPlatformDriver sdl2Driver;
#endif /* LIBUNR_SDL2 */

#if defined BUILD_X11
class X11GlPlatformContext: public GlPlatformContext
{
public:
	using SharedContext = X11GlPlatformDriver::SharedContext;

	X11GlPlatformContext(UX11Viewport * viewport, SharedContext & context):
		viewport(viewport),
		glContext(**context),
		context(context)
	{}

	bool LoadExtensions() override
	{
		return LoadExtensionsImpl(X11GlPlatformContext::GetProcAddress);
	}

	void Swap() override
	{
		glXSwapBuffers(viewport->m_Display, viewport->m_Window);
	}

	bool MakeCurrent() override
	{
		return glXMakeCurrent(viewport->m_Display,
		                      viewport->m_Window,
		                      glContext) == True;
	}

	virtual ~X11GlPlatformContext() {}

protected:
	static void * GetProcAddress(const char * name)
	{
		return reinterpret_cast<void *>(glXGetProcAddress(reinterpret_cast<const GLubyte *>(name)));
	}

	UX11Viewport * viewport;
	GLXContext     glContext;
	SharedContext  context;
};

X11GlPlatformContext * X11GlPlatformDriver::CreateMainContext(UOpenGL3RenderDevice * device, UX11Viewport * viewport, SharedContext & mainContext)
{
	GLint attributes[] = { GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None };

	auto window = viewport->m_Window;
	auto display = viewport->m_Display;
	auto visual = glXChooseVisual(display, 0, attributes);
	if (visual == NULL)
	{
		GLogf(LOG_CRIT, "Couldn't select glX visual");
		return nullptr;
	}

	auto glContext = glXCreateContext(display, visual, NULL, True);
	if (glXMakeCurrent(display, window, glContext) != True)
	{
		GLogf(LOG_CRIT, "Couldn't make glX context current");
		return nullptr;
	}

	mainContext = std::make_shared<X11RawContext>(glContext, display);

	return new X11GlPlatformContext(viewport, mainContext);
}

X11GlPlatformContext * X11GlPlatformDriver::CreateSubordinateContext(UOpenGL3RenderDevice * device, UX11Viewport * viewport, SharedContext & sharedContext)
{
	return new X11GlPlatformContext(viewport, sharedContext);
}

X11GlPlatformDriver x11Driver;
#endif /* BUILD_X11 */

template <typename TDriver, typename TViewport, typename TRawContext>
bool SharedContextGlPlatformDriver<TDriver, TViewport, TRawContext>::Match(UViewport * viewport)
{
	TViewport * downcastViewport = dynamic_cast<TViewport *>(viewport);
	if (downcastViewport != nullptr)
	{
		return true;
	}

	GLogf(LOG_WARN , "%s OpenGL driver not appropriate for '%s'",
		  name.c_str(), viewport->Class->Name.Data());

	return false;
}

template <typename TDriver, typename TViewport, typename TRawContext>
GlPlatformContext * SharedContextGlPlatformDriver<TDriver, TViewport, TRawContext>::Create(UOpenGL3RenderDevice * device, UViewport * genericViewport)
{
	TViewport * viewport = dynamic_cast<TViewport *>(genericViewport);
	if (viewport == nullptr)
	{
		GLogf(LOG_ERR, "Create %s OpenGL Context for '%s'",
			  name.c_str(), genericViewport->Class->Name.Data());
		return nullptr;
	}

	if (auto sharedContext = mainContext.lock())
	{
		GLogf(LOG_INFO, "Creating shared OpenGL context");
		return driver().CreateSubordinateContext(device, viewport, sharedContext);
	}

	SharedContext sharedContext(nullptr);
	auto context = driver().CreateMainContext(device, viewport, sharedContext);
	if (!context)
	{
		GLogf(LOG_ERR, "Failed to create %s OpenGL context", name.c_str());
		return nullptr;
	}

	mainContext = sharedContext;

	context->MakeCurrent();

	if (!context->LoadExtensions())
	{
		delete context;
		return nullptr;
	}

	if (!device->CompileShaderPrograms())
	{
		 delete context;
		 return nullptr;
	}

	return context;
}
