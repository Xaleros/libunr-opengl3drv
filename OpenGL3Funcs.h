/*===========================================================================*\
|*  libunr-OpenGL3Drv - An OpenGL3 Render Device for libunr                  *|
|*  Copyright (C) 2018-2019  Adam W.E. Smith                                 *|
|*                                                                           *|
|*  This program is free software: you can redistribute it and/or modify     *|
|*  it under the terms of the GNU General Public License as published by     *|
|*  the Free Software Foundation, either version 3 of the License, or        *|
|*  (at your option) any later version.                                      *|
|*                                                                           *|
|*  This program is distributed in the hope that it will be useful,          *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *|
|*  GNU General Public License for more details.                             *|
|*                                                                           *|
|*  You should have received a copy of the GNU General Public License        *|
|*  along with this program. If not, see <https://www.gnu.org/licenses/>.    *|
\*===========================================================================*/

/*========================================================================
 * OpenGL3Funcs.h - OpenGL extensions used by the renderer
 *
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#pragma once

#if defined LIBUNR_WIN32
  #include <Windows.h>
#endif

#include "GL/gl.h"
#include "glext.h"

#if defined LIBUNR_WIN32
  #include "wgl.h"
  #include "wglext.h"
#endif

bool LoadExtensionsImpl(void* (*GetProcAddress)(const char *));

// Buffer extensions
extern PFNGLGENBUFFERSPROC glGenBuffers;
extern PFNGLBINDBUFFERPROC glBindBuffer;
extern PFNGLBUFFERDATAPROC glBufferData;
extern PFNGLDELETEBUFFERSPROC glDeleteBuffers;

// Texture extensions
#if defined LIBUNR_WIN32
extern PFNGLACTIVETEXTUREPROC glActiveTexture;
#endif /* defined LIBUNR_WIN32 */

// Blending extensions
extern PFNGLBLENDFUNCSEPARATEPROC glBlendFuncSeparate;
#if defined LIBUNR_WIN32
extern PFNGLBLENDEQUATIONPROC     glBlendEquation;
#endif /* defined LIBUNR_WIN32 */

// Vertex array extensions
extern PFNGLGENVERTEXARRAYSPROC glGenVertexArrays;
extern PFNGLBINDVERTEXARRAYPROC glBindVertexArray;
extern PFNGLDELETEVERTEXARRAYSPROC glDeleteVertexArrays;

// Drawing extensions
#if defined LIBUNR_WIN32
extern PFNGLDRAWRANGEELEMENTSPROC glDrawRangeElements;
#endif /* defined LIBUNR_WIN32 */
extern PFNGLPRIMITIVERESTARTINDEXPROC glPrimitiveRestartIndex;

// Shader extensions
extern PFNGLCREATESHADERPROC      glCreateShader;
extern PFNGLSHADERSOURCEPROC      glShaderSource;
extern PFNGLCOMPILESHADERPROC     glCompileShader;
extern PFNGLCREATEPROGRAMPROC     glCreateProgram;
extern PFNGLISPROGRAMPROC         glIsProgram;
extern PFNGLATTACHSHADERPROC      glAttachShader;
extern PFNGLDELETESHADERPROC      glDeleteShader;
extern PFNGLGETSHADERIVPROC       glGetShaderiv;
extern PFNGLGETSHADERINFOLOGPROC  glGetShaderInfoLog;
extern PFNGLLINKPROGRAMPROC       glLinkProgram;
extern PFNGLUSEPROGRAMPROC        glUseProgram;
extern PFNGLDELETEPROGRAMPROC     glDeleteProgram;
extern PFNGLGETPROGRAMIVPROC      glGetProgramiv;
extern PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog;

// Vertex attribute extensions
extern PFNGLVERTEXATTRIBPOINTERPROC     glVertexAttribPointer;
extern PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray;
extern PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray;

// Shader uniform extensions
extern PFNGLUNIFORM1IPROC glUniform1i;
extern PFNGLUNIFORM3FVPROC glUniform3fv;
extern PFNGLUNIFORM4FPROC glUniform4f;
extern PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv;
extern PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation;
