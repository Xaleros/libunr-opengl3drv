/*===========================================================================*\
|*  libunr-OpenGL3Drv - An OpenGL3 Render Device for libunr                  *|
|*  Copyright (C) 2018-2019  Adam W.E. Smith                                 *|
|*                                                                           *|
|*  This program is free software: you can redistribute it and/or modify     *|
|*  it under the terms of the GNU General Public License as published by     *|
|*  the Free Software Foundation, either version 3 of the License, or        *|
|*  (at your option) any later version.                                      *|
|*                                                                           *|
|*  This program is distributed in the hope that it will be useful,          *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *|
|*  GNU General Public License for more details.                             *|
|*                                                                           *|
|*  You should have received a copy of the GNU General Public License        *|
|*  along with this program. If not, see <https://www.gnu.org/licenses/>.    *|
\*===========================================================================*/

/*========================================================================
 * OpenGL3RenderDevice.h - OpenGL 2.x 3D rendering device
 *
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#pragma once

#include <Util/TQueue.h>
#include <Core/UMath.h>
#include <Engine/UMesh.h>
#include <Engine/URender.h>
#include <Engine/UTexture.h>

#include "OpenGL3Funcs.h"
#include "OpenGL3Shader.h"

#undef DrawText

enum EProjection
{
  PROJ_Perspective,
  PROJ_Ortho
};

enum EElementType
{
  ELEM_Tile,
  ELEM_VertexMesh,
  ELEM_SkeletalMesh,
  ELEM_StaticMesh,
  ELEM_Bsp,
};

enum EMeshType
{
  MESH_Old,
  MESH_Vertex,
  MESH_Skeletal,
  MESH_Static,
  MESH_Primitive,
};

class GlPlatformContext;

/*-----------------------------------------------------------------------------
 * FGLViewport
 * A viewport tied together with a draw context and OpenGL context
-----------------------------------------------------------------------------*/
struct FGLViewport
{
  UViewport* Viewport;
  FMatrix4x4 Orthographic;
  FMatrix4x4 Perspective;
  EProjection Matrix;

  GlPlatformContext * context;

  FGLViewport& operator=( FGLViewport& Rhs ) 
  {
    memcpy( this, &Rhs, sizeof( FGLViewport ) );
    return *this;
  }
};

/*-----------------------------------------------------------------------------
 * FVertexUV
 * A vertex stored with UV coordinates
-----------------------------------------------------------------------------*/
struct FVertexUV
{
  float X;
  float Y;
  float Z;
  float U;
  float V;
};

template <> struct TDestructorInfo<FVertexUV> { static FORCEINLINE bool NeedsDestructor() { return false; } };

/*-----------------------------------------------------------------------------
 * FCanvasElement
 * A queued element to be drawn on the canvas after DrawWorld
-----------------------------------------------------------------------------*/
struct FCanvasElement
{
  FCanvasElement();
  FCanvasElement( const FCanvasElement& E );
  ~FCanvasElement();

  EElementType Type;
  UTexture* Texture;          // The texture that will be drawn with this element
  int VertexShader;           // The vertex shader to be used with this element
  int FragmentShader;         // The fragment shader to be used with this element
  int PolyFlags;              // Poly flags that determine fragment shader / blend state

  // Tile data
  TArray<FVertexUV> Vertices; // The vertex coordinates to be drawn
  TArray<u32> Indices;        // A list of vertex coordinate indices

  // Mesh data
  UMesh*   Mesh;              // The mesh to be drawn
  float    AnimFrame;         // Current animation frame between 0.0-1.0
  u32      AnimVerts;         // The number of verts per anim frame
  FVector  Scale;
  FRotator Rotation;
  FVector  Translation;
};

template <> struct TDestructorInfo<FCanvasElement> { static FORCEINLINE bool NeedsDestructor() { return false; } };

/*-----------------------------------------------------------------------------
 * FMeshElement
 * A mesh to draw arbitrarily in the world
-----------------------------------------------------------------------------*/
struct FMeshElement
{
  FMeshElement();
  FMeshElement( const FMeshElement& E );
  ~FMeshElement();

  EMeshType Type;

  // Fragment data
  UTexture* Texture;          // The texture that overrides the mesh texture
  int VertexShader;           // The vertex shader to be used with this element
  int FragmentShader;         // The fragment shader to be used with this element
  int PolyFlags;              // Poly flags that determine fragment shader / blend state

  // Mesh data
  float    AnimFrame;         // Current animation frame between 0.0-1.0
  u32      AnimVerts;         // The number of verts per anim frame
  FVector  Scale;
  FRotator Rotation;
  FVector  Translation;

  // Vertex data
  TArray<FVertexUV> Vertices; // The vertex coordinates to be drawn
  TArray<u32> Indices;        // A list of vertex coordinate indices
};

/*-----------------------------------------------------------------------------
 * FBspPolySet
 * A set of BSP surfaces with the same texture and material grouped up
 * to be buffered in a single pass
-----------------------------------------------------------------------------*/
struct FBspPolySet
{
  UTexture* Texture;
  int FragmentShader;
  int PolyFlags;
  TArray<FVertexUV> Vertices;
  TArray<u32> Indices;
};

/*-----------------------------------------------------------------------------
 * FTransparentElement
 * A set of polygons that are transparent and must be drawn in a particular
 * order, or else the z-buffer will prevent true transparency
-----------------------------------------------------------------------------*/
struct FTransparentElement
{
  int ElementType;

  // General data
  float Distance;
  UTexture* Texture;
  int VertexShader;
  int FragmentShader;
  int PolyFlags;
  TArray<FVertexUV> Vertices;
  TArray<u32> Indices;

  // Mesh data
  float    AnimFrame;         // Current animation frame between 0.0-1.0
  u32      AnimVerts;         // The number of verts per anim frame
  FVector  Scale;
  FRotator Rotation;
  FVector  Translation;
};

/*-----------------------------------------------------------------------------
 * UOpenGL3RenderDevice
 * The main OpenGL 3.x rendering subsystem
-----------------------------------------------------------------------------*/
class DLL_EXPORT UOpenGL3RenderDevice : public URenderDevice
{
  DECLARE_NATIVE_CLASS( UOpenGL3RenderDevice, URenderDevice, CLASS_NoExport, OpenGL3Drv )
  UOpenGL3RenderDevice();

  // USubsystem functions
  virtual bool Init();
  virtual bool Exit();
  virtual void Tick( float DeltaTime );

  // URenderDevice functions
  virtual void DrawBspSurface( UModel* Model, FBspNode& Node, UViewport* Viewport );
  virtual void DrawActor( AActor* Actor );
  virtual void DrawText( UFont* Font, FBoxInt2D& Dim, FString& Text, int PolyFlags = 0 );
  virtual void DrawTile( UTexture* Tex, FBoxInt2D& Dim, FRotator& Rot, float U, float V, float UL, float VL, int PolyFlags = 0 );
  virtual void DrawMesh( UMesh* Mesh, FMeshAnimSeq& AnimSeq, float AnimFrame, FVector& Loc, FVector& Scale, FRotator& Rot, int PolyFlags = 0 );
  virtual void DrawCube( FVector& Loc, FRotator& Rot, FVector& Scale, UTexture* Tex );
  virtual void DrawGrid( FBox& Dim, FColor& Color );
  virtual bool InitViewport( UViewport* Viewport );
  virtual bool SetActiveViewport( UViewport* Viewport );

  // OpenGL specific functions
  bool GetExtensions();
  bool RegisterTexture( UTexture* Texture );
  bool UnregisterTexture( UTexture* Texture );
  bool CompileShaderPrograms();
  bool UpdateTexture( UTexture* Texture );
  FGLShaderProgram& SetPolygonEffects( UTexture* Texture, int VertexShader, int FragmentShader, int PolyFlags );

  // Transparency rendering functions
  void DrawTransparentBspSurface( UModel* Model, FBspNode& Node, UViewport* Viewport );
  void DrawTransparentMesh( UMesh* Mesh, FMeshAnimSeq& AnimSeq, float AnimFrame, FVector& Loc, FVector& Scale, FRotator& Rot, int PolyFlags );
  void RenderTransparentElements();

  // Bsp rendering
  void RenderBspElements();

  // Mesh rendering
  void RenderMeshElements();
  void RenderPrimitiveMesh( FMeshElement& Element, int BufIdx );
  void RenderOldMesh( FMeshElement& Element, int BufIdx ) {}
  void RenderLodMesh( FMeshElement& Element, int BufIdx );

  // Canvas rendering
  void RenderCanvasElements();
  void RenderTileElement( FCanvasElement& Element, int BufIdx );
  void RenderVertexMeshElement( FCanvasElement& Element, int BufIdx );

  // Per-device render elements
  FGLViewport* CurrentViewport;
  TArray<FGLViewport> Viewports;      // All viewports with rendering contexts
  FGLShaderManager ShaderManager;

  // Per-tick render elements
  int NumBuffers; // The number of buffers needed this frame
  int CurrentBuffer;
  TArray<FBspPolySet> BspPolyData;
  TArray<FTransparentElement> TransparentPolyData; // Transparent polygons must be drawn last
  TQueue<FCanvasElement> CanvasDrawList; // A list of Canvas->Draw*() elements
  TQueue<FMeshElement>   MeshDrawList;   // A list of arbitrarily drawn meshes
  TArray<GLuint> VertexBuffers;
  TArray<GLuint> IndexBuffers;
};
