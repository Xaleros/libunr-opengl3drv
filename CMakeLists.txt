cmake_minimum_required(VERSION 3.10)
project(UNRGL3 CXX)

set(CMAKE_CXX_STANDARD 11)

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake/Modules" ${CMAKE_MODULE_PATH})

set(UNR_INC ${CMAKE_CURRENT_SOURCE_DIR})
set(UNR_SRC ${CMAKE_CURRENT_SOURCE_DIR})

add_definitions("-DINSTALL_PREFIX=\"${CMAKE_INSTALL_PREFIX}\"")
message(STATUS "Install prefix: " ${CMAKE_INSTALL_PREFIX})

set(MODULES_INSTALL_DIR "lib/libunr/modules")

find_package(OpenGL REQUIRED OpenGL
             OPTIONAL_COMPONENTS GLX)

# When doing a top level build we don't want to do find_package
if (NOT TARGET Unr::Unr)
	find_package(Unr REQUIRED)
endif()

add_library(OpenGL3Drv SHARED
    ${UNR_SRC}/OpenGL3Funcs.cpp
    ${UNR_SRC}/OpenGL3RenderDevice.cpp
    ${UNR_SRC}/OpenGL3Shader.cpp
    ${UNR_SRC}/OpenGL3Platform.cpp
)

target_include_directories(OpenGL3Drv
	PRIVATE
		${OPENGL_INCLUDE_DIR}
)

set_target_properties(OpenGL3Drv
	PROPERTIES
		PREFIX ""
)

target_link_libraries(OpenGL3Drv
	${OPENGL_LIBRARIES}
	Unr::Unr
)

install(
	TARGETS
		OpenGL3Drv
	LIBRARY
		DESTINATION ${MODULES_INSTALL_DIR}
)

if (NOT WIN32)
	set(LIBUNR_SHADER_INSTALL_PATH ${CMAKE_INSTALL_PREFIX}/share/libunr/modules/OpenGL3)
else()
	set(LIBUNR_SHADER_INSTALL_PATH ${CMAKE_INSTALL_PREFIX}/bin/OpenGL3)
endif()

install(
	FILES
		${UNR_SRC}/shaders/DefaultShader.frag
		${UNR_SRC}/shaders/DefaultShader.vert
		${UNR_SRC}/shaders/DetailTexShader.frag
		${UNR_SRC}/shaders/ModulatedShader.frag
		${UNR_SRC}/shaders/TranslucentShader.frag
	DESTINATION
		${LIBUNR_SHADER_INSTALL_PATH}
)
